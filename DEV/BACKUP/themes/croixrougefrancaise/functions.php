<?php

// Inclusion des classes

include_once 'CRF.php';
include_once 'CRF_Menu.php';
include_once 'CRF_Filters.php';

include_once 'admin/functions.php';


load_theme_textdomain('redux-framework', get_template_directory() . '/languages' );

register_nav_menu('sections', __('Rubriques du site'));


/**
 * Récupère la valeur d'une option du paramètrage spécifique
 * @param string $setting_name
 * @param string $default_value
 * @return Ambigous <string, string, mixed>
 */
function crf_get_option($setting_name, $default_value = '')
{
	return CRF::get_option($setting_name, $default_value);
}
