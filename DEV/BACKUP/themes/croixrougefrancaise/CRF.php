<?php

/**
 * Thème Croix-Rouge
 * 
 * @author fraoult
 *
 */
class CRF
{
	
	/**
	 * Ajout des JS
	 */
	static function js()
	{
		wp_enqueue_script('crf_js', get_stylesheet_directory_uri().'/js/croixrouge.js');
	}

	/**
	 * ADMIN - Ajout des JS
	 */
	static function admin_js()
	{
		wp_enqueue_script('gapi_place', 'https://maps.googleapis.com/maps/api/js?libraries=places');
		wp_enqueue_script('crf_admin_js', get_stylesheet_directory_uri().'/js/admin.js');
	}

	/**
	 * ADMIN - Ajout des styles
	 */
	static function admin_css()
	{
		wp_enqueue_style( 'crf_admin_css', get_stylesheet_directory_uri().'/css/admin.css');
	}

	/**
	 * Login
	 */
	static function login_head()
	{
		self::admin_css();
	}


	/**
	 * Méthode - Récupération de la config spécifique CRF
	 * @param string $setting_name
	 * @param string $default
	 * @return string
	 */
	static function get_option($setting_name, $default='')
	{
		if ($setting_name === 'nom_ul_avec_preposition')
		{
			return self::get_title_ul();
		}
		$opt = get_option('crf_ul_settings');
		return isset($opt[$setting_name]) ? $opt[$setting_name] : $default;
	}
	
	/**
	 * Retourne le nom de l'UL précédé par sa préposition, avec ajout ou non de l'espace
	 * @return string
	 */
	static function get_title_ul()
	{
		$preposition = trim(crf_get_option('preposition_nom_ul'));
		$nom = trim(crf_get_option('nom_ul'));
		if ($preposition{strlen($preposition)-1}=='\'')
		{
			return $preposition.$nom;
		}
		else
		{
			return $preposition.' '.$nom; 
		}
	}
	
} // END CLASS


add_action('wp_footer', 'CRF::js');
add_action('admin_enqueue_scripts', 'CRF::admin_js');
add_action('admin_print_styles', 'CRF::admin_css');
add_action('login_head', 'CRF::login_head');
