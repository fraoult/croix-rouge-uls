/* ================================================= */
/*                API GOOGLE MAP                     */ 
/* ================================================= */

/**
 * Autocomplétion GoogleMap
 */
var GoogleMap = function(inputElement, listener)
{
	if (! GoogleMap.isLoaded())
	{
		console.error('Google API is not loaded !');
		return;
	}
	this.autocomplete = new google.maps.places.Autocomplete(inputElement, {types : ['geocode'], componentRestrictions: {country: 'fr'}});
	if (listener)
	{
		var autocomplete = this.autocomplete;
		this.autocomplete.addListener('place_changed', function(){listener(inputElement, autocomplete, GoogleMap.formatLocation(autocomplete))});
	}
};


/**
 * Bias the autocomplete object to the user's geographical location,
 * as supplied by the browser's 'navigator.geolocation' object.
 */
GoogleMap.prototype.geolocate = function()
{
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(position){
			var geolocation = {
				lat : position.coords.latitude,
				lng : position.coords.longitude
			};
			var circle = new google.maps.Circle({
				center : geolocation,
				radius : position.coords.accuracy
			});
			this.autocomplete.setBounds(circle.getBounds());
		});
	}
}

/**
 * Address components
 */
GoogleMap.addressComponents = {
	street_number : 'long_name',
	route : 'long_name',
	postal_code : 'long_name',
	locality : 'long_name',
//	administrative_area_level_1 : 'short_name',
//	country : 'long_name',
}

/**
 * API is loaded ?
 */

GoogleMap.isLoaded = function()
{
	return (typeof(google) != 'undefined' && google.maps != undefined);
}

GoogleMap.formatLocation = function(autocomplete)
{
	var place = autocomplete.getPlace();
	var result = {};
	for (var i = 0; i < place.address_components.length; i++)
	{
		var addressType = place.address_components[i].types[0];
		result[addressType] = place.address_components[i]["long_name"];
	}
	return result;
}

/* ================================================= */
/* ================================================= */


/**
 * Personnalisation de l'interface d'admin
 */

jQuery(document).ready(function ($) {
  
	// ------------------------------------------
	// Verrouiller certains champs
	// ------------------------------------------
	
	$('#blogname').attr('readonly','readonly');
	$('#blogdescription').attr('readonly','readonly');
	$('#timezone_string').attr('readonly','readonly');
	$('input[name="date_format"]').attr('readonly','readonly');
	$('#date_format_custom').attr('readonly','readonly');
	$('input[name="time_format"]').attr('readonly','readonly');
	$('#time_format_custom').attr('readonly','readonly');
	$('#start_of_week').attr('readonly','readonly');
	$('#WPLANG').attr('readonly','readonly');
	
	$('select[readonly=readonly]').each(function(){
		$this = $(this); 
		$('<input>')
			.attr({
				'type' : 'hidden',
				'name' : $this.attr('name'),
				'value': $this.val()
			})
			.insertAfter($this);
		$this.attr('disabled', 'disabled');
	});
	  
	$('#unitelocale').parents('tr').detach().insertAfter($('#blogdescription').parents('tr'));

	// ------------------------------------------
	// Autocomplétion Google Map
	// ------------------------------------------

	var autocomplete_listener = function(inputElement, autocomplete, address)
	{
		$(inputElement).val(((address.street_number||'')+' '+(address.route||'')+' '+(address.postal_code||'')+' '+(address.locality||'')).trim());
	}

	$('section[data-type="etablissements"]').find('input').each(function(){
		new GoogleMap(this, autocomplete_listener);
	});

	new GoogleMap($('#crf_adresse_ul').get(0), autocomplete_listener);
	
	
	// ------------------------------------------
	// Ajouter et supprimer un établissement
	// ------------------------------------------

	// ID le plus grand
	
	var etablissement_last_index = 0;
	$('section[data-type="etablissements"]').each(function(){
		var idx = $(this).attr('data-index');
		if (idx > etablissement_last_index)
		{
			etablissement_last_index = idx;
		}
	});
	
	
	// Fonction de suppression
	
	var func_remove = function()
	{
		var id_ref = $(this).attr('data-ref');
		$('section[data-type="etablissements"][data-index="'+id_ref+'"]').remove();
		
		var i = -1;
		$('section[data-type="etablissements"]').each(function(){
			i++;
			$(this).attr('data-index', i);
			$(this).find('select').attr('name', 'crf_ul_settings[etablissements]['+i+'][type]').attr('id', 'crf_etablissement_ul_'+new_index);
			$(this).find('input[type="text"]').attr('name', 'crf_ul_settings[etablissements]['+i+'][adresse]');
			$(this).find('input[type="tel"]').attr('name', 'crf_ul_settings[etablissements]['+i+'][tel]');
			$(this).find('input[type="email"]').attr('name', 'crf_ul_settings[etablissements]['+i+'][mail]');
			$(this).find('textaera').attr('name', 'crf_ul_settings[etablissements]['+i+'][extra]');
			$(this).find('button').attr('data-ref', i);
		});
		etablissement_last_index = i;
		new_index = i + 1;
	};

	var new_index = etablissement_last_index + 1;
	var section_template = $('section[data-type="etablissements"][data-index="0"]');

	// Fonction d'ajout

	$('#crf_etablissements_ul_ajouter').click(function(){
		var new_section = section_template.clone();
		new_section.attr('data-index', new_index);
		new_section.find('select').attr('name', 'crf_ul_settings[etablissements]['+new_index+'][type]').attr('id', 'crf_etablissement_ul_'+new_index).val([]).find('option:selected').removeAttr('selected').prop('selected', false);
		new_section.find('input[type="text"]').attr('name', 'crf_ul_settings[etablissements]['+new_index+'][adresse]').val('');
		new_section.find('input[type="email"]').attr('name', 'crf_ul_settings[etablissements]['+new_index+'][mail]').val('');
		new_section.find('input[type="tel"]').attr('name', 'crf_ul_settings[etablissements]['+new_index+'][tel]').val('');
		new_section.find('textarea').val('').text('');
		new_section.find('button').attr('data-ref', new_index).click(func_remove);
		new GoogleMap(new_section.find('input').get(0), autocomplete_listener);
		$(this).before(new_section);
		new_index++;
	});
	
	// Suppression
	
	$('button[data-type="suppr_etablissement"]').click(func_remove);
	
	

});


