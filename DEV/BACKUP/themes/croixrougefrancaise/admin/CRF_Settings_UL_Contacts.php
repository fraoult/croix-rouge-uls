<?php

/**
 * Configuration spécifique à l'UL
 * @author fraoult
 *
 */
class CRF_Settings_UL_Contacts
{

	/**
	 * Liste des établissements
	 * @var array
	 */
	public static $contact_list = array(
			'volontariat'  => 'Volontariat',
			'urgence'      => 'Urgence',
			'secourisme'   => 'Equipe secouriste',
			'social'       => 'Equipe sociale',
			'formation'    => 'Formation',
			'commentaire'  => 'Commentaires',
			'don'          => 'Don',
	);

	/**
	 * Ajouter le lien au menu
	*/
	static function add_admin_menu()
	{
		add_submenu_page('unitelocale', 'Contacts de l\'unité locale', 'Contacts', 'manage_options', 'unitelocale_contacts', __CLASS__.'::options_page' );
	}

	/**
	 * Callback concernant la section
	 */
	static function section_callback()
	{
		//echo __( 'This section description', 'crf' );
	}


	/**
	 * Création de la page
	 */
	static function options_page()
	{
		?>
			<form action='options.php' method='post'>
				
				<h2>Contacts de l'unité locale</h2>
				<p>Indiquer ici les différents contacts</p>
				<p>Il n'est pas nécessaire d'indiquer tous les champs !</p>
				<?php
				settings_fields( 'unitelocale_contacts' );
				do_settings_sections( 'unitelocale_contacts' );
				submit_button();
				?>
				
			</form>
			<?php
		
		}
		
	/**
	 * Init : Ajouter la section, description, champs...
	 */
	static function init()
	{
		register_setting('unitelocale_contacts', 'crf_ul_settings');
	
		add_settings_section(
			'crf_ul_settings_section',
			'',//__( 'Your section description', 'crf' ),
			__CLASS__.'::section_callback',
			'unitelocale_contacts'
		);

		add_settings_field(
			'crf_contact_ul',
			__( 'Unité locale', 'crf' ),
			__CLASS__.'::contact_render_ul',
			'unitelocale_contacts',
			'crf_ul_settings_section',
			array('label_for' => 'crf_contact_ul_tel')
		);

		add_settings_field(
			'crf_contact_webmaster',
			__( 'Webmaster', 'crf' ),
			__CLASS__.'::contact_render_webmaster',
			'unitelocale_contacts',
			'crf_ul_settings_section',
			array('label_for' => 'crf_contact_ul_tel')
		);
		
		foreach(self::$contact_list as $type => $title)
		{
			add_settings_field(
				'crf_contact_'.$type,
				__( $title, 'crf' ),
				__CLASS__.'::contact_render',
				'unitelocale_contacts',
				'crf_ul_settings_section',
				array('label_for' => 'crf_contact_'.$type.'_tel', 'contact_type' => $type)
			);
		}
		
	}
	
	/**
	 * Render des contacts
	 */
	static function contact_render($args)
	{
		$type = $args['contact_type'];
		$options = get_option( ' crf_ul_settings' );
		$contact_list= $options['contacts'];
		if ($contact_list === NULL)
		{
			$contact_list = array();
		}
		$contact = isset($contact_list[$type]) ? $contact_list[$type] : array('tel' => '', 'mail' => '');
		?>
			<input type="tel"  id="crf_contact_'<?php echo $type?>_tel" name="crf_ul_settings[contacts][<?php echo $type?>][tel]" placeholder="Téléphone" value="<?php echo $contact['tel'] ?>" maxlength="16" />
			<input type="email" id="crf_contact_'<?php echo $type?>_mail" name="crf_ul_settings[contacts][<?php echo $type?>][mail]" placeholder="Courriel" value="<?php echo $contact['mail'] ?>" />
		<?php 
	}
	
	/**
	 * Render contact UL
	 */
	static function contact_render_ul()
	{
		$contact_tel = crf_get_option('telephone');
		$contact_mail = crf_get_option('mail');
		?>
				<input type="tel"   placeholder="Téléphone" value="<?php echo $contact_tel ?>" maxlength="16" readonly="readonly" disabled="disabled" /> 
				<input type="email" placeholder="Courriel" value="<?php echo $contact_mail ?>"  readonly="readonly" disabled="disabled" />
				<span>(cf. page de configuration de l'UL)</span>
		<?php 
	}

	
	/**
	 * Render contact webmaster
	 */
	static function contact_render_webmaster()
	{
		$contact_tel = crf_get_option('telephone_webmaster');
		$contact_mail = crf_get_option('mail_webmaster');
		?>
				<input type="tel"   placeholder="Téléphone" value="<?php echo $contact_tel ?>" maxlength="16" readonly="readonly" disabled="disabled" />
				<input type="email" placeholder="Courriel" value="<?php echo $contact_mail ?>"  readonly="readonly" disabled="disabled" />
				<span>(cf. page de configuration de l'UL)</span>
		<?php 
	}
		
		
} // END CLASS

add_action( 'admin_menu', 'CRF_Settings_UL_Contacts::add_admin_menu', 11);
add_action( 'admin_init', 'CRF_Settings_UL_Contacts::init', 11);
