<?php

/**
 * Configuration spécifique à l'UL
 * @author fraoult
 *
 */
class CRF_Settings_UL_Etablissements
{

	/**
	 * Liste des établissements
	 * @var array
	 */
	public static $etablissements_list = array(
			'secourisme'    => array('title'=> 'Equipe secouriste', 'icon' => 'crf-ul.png'),
			'vestiaire'     => array('title'=> 'Vestiaire', 'icon' => 'crf-vestiaire.png'),
			'vestiboutique' => array('title'=> 'Vestiboutique', 'icon' => 'crf-vestiboutique.png'),
			'epicerie'      => array('title'=> 'Epicerie sociale', 'icon' => 'crf-epicerie.png'),
			'accueil-jour'  => array('title'=> 'Accueil de S.D.F. de jour', 'icon' => 'crf-accueiljour.png'),
			'aide-alim'     => array('title'=> 'Aide alimentaire', 'icon'=>'crf-aidealimentaire.png'),
			'bebe-maman'    => array('title'=> 'Espace bébé-mamans', 'icon' => 'crf-bebemaman.png'),
			'pmi'           => array('title'=> 'PMI / RAM', 'icon' => 'crf-bebemaman.png'),
	);

	/**
	 * Ajouter le lien au menu
	*/
	static function add_admin_menu()
	{
		add_submenu_page('unitelocale', 'Etablissements de l\'unité locale', 'Etablissements', 'manage_options', 'unitelocale_etablissements', __CLASS__.'::options_page' );
	}

	/**
	 * Callback concernant la section
	 */
	static function section_callback()
	{
		//echo __( 'This section description', 'crf' );
	}


	/**
	 * Création de la page
	 */
	static function options_page()
	{
		?>
			<form action='options.php' method='post'>
				
				<h2>&Eacute;tablissements de l'unité locale</h2>
				<p>Indiquer ici les différents établissements gérés par l'unité locale (Vestiboutique, épicerie, etc.)</p>
				<p>Les adresses indiquées permettrons aux établissements d'être pointées sur la carte</p>
				
				<?php
				settings_fields( 'unitelocale_etablissements' );
				do_settings_sections( 'unitelocale_etablissements' );
				submit_button();
				?>
				
			</form>
		<?php
	}
		
	/**
	 * Init : Ajouter la section, description, champs...
	 */
	static function init()
	{
		register_setting('unitelocale_etablissements', 'crf_ul_settings');
	
		add_settings_section(
			'crf_ul_settings_section',
			'',//__( 'Your section description', 'crf' ),
			__CLASS__.'::section_callback',
			'unitelocale_etablissements'
		);

		add_settings_field(
			'crf_etablissements_ul',
			__( 'Etablissements', 'crf' ),
			__CLASS__.'::etablissement_ul_render',
			'unitelocale_etablissements',
			'crf_ul_settings_section',
			array('label_for' => 'crf_etablissements_ul_0')
		);
		
	}
	
	/**
	 * Render des établissements
	 */
	static function etablissement_ul_render()
	{
		$options = get_option( ' crf_ul_settings' );
		$etablissement_list = $options['etablissements'];
		if ($etablissement_list === NULL)
		{
			$etablissement_list = array();
		}
		?>
		
		<?php foreach($etablissement_list as $i => $etablissement) : ?>
			<section data-type="etablissements" data-index="<?php echo $i?>">
				<select id="crf_etablissements_ul_<?php echo $i?>" name="crf_ul_settings[etablissements][<?php echo $i?>][type]" required="required" />
					<optgroup label="Type d'établissement"/>
						<?php foreach(self::$etablissements_list as $value => $infos): ?>
						<option value="<?php echo $value?>" <?php if ($etablissement['type']==$value) echo 'selected="selected"'?>><?php echo $infos['title'] ?></option>
						<?php endforeach; ?>
					</optgroup>
				</select>
				<input type="text" name="crf_ul_settings[etablissements][<?php echo $i?>][adresse]" placeholder="Adresse de l'établissement" value="<?php echo $etablissement['adresse'] ?>" />
				<input type="tel" name="crf_ul_settings[etablissements][<?php echo $i?>][tel]" placeholder="Téléphone de l'établissement" value="<?php echo $etablissement['tel'] ?>" maxlength="16" />
				<input type="email" name="crf_ul_settings[etablissements][<?php echo $i?>][mail]" placeholder="Courriel de l'établissement" value="<?php echo $etablissement['mail'] ?>" />
				<textarea name="crf_ul_settings[etablissements][<?php echo $i?>][extra]" placeholder="Informations diverses (horaires d'ouverture, etc.)"><?php echo $etablissement['extra'] ?></textarea>
				<button type="button" data-type="suppr_etablissement" data-ref="<?php echo $i?>">Supprimer</button>
			</section>
		<?php endforeach; ?>
		<?php  if (empty($etablissement_list)) :?>
			<section data-type="etablissements" data-index="0">
				<select id="crf_etablissements_ul_0" name="crf_ul_settings[etablissements][0][type]"/>
					<optgroup label="Type d'établissement"/>
						<?php foreach(self::$etablissements_list as $value => $infos): ?>
						<option value="<?php echo $value?>"><?php echo $infos['title'] ?></option>
						<?php endforeach; ?>
					</optgroup>
				</select>
				<input type="text" name="crf_ul_settings[etablissements][0][adresse]" placeholder="Adresse de l'établissement" />
				<input type="tel" name="crf_ul_settings[etablissements][0][tel]" placeholder="Téléphone de l'établissement" maxlength="16" />
				<input type="email" name="crf_ul_settings[etablissements][0][mail]" placeholder="Courriel de l'établissement" />
				<textarea name="crf_ul_settings[etablissements][0][extra]" placeholder="Informations diverses (horaires d'ouverture, etc.)"></textarea>
				<!-- <button type="button" data-type="suppr_etablissement" data-ref="0">Supprimer</button> -->
			</section>
		<?php endif; ?>
		<button type="button" id="crf_etablissements_ul_ajouter">Ajouter un établissement</button>
		
		<?php 
	}
	
} // END CLASS

add_action( 'admin_menu', 'CRF_Settings_UL_Etablissements::add_admin_menu', 11);
add_action( 'admin_init', 'CRF_Settings_UL_Etablissements::init', 11);
