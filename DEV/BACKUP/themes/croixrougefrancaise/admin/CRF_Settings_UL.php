<?php

/**
 * Configuration spécifique à l'UL
 * @author fraoult
 *
 */
class CRF_Settings_UL
{
	/**
	 * Ajouter le lien au menu
	*/
	static function add_admin_menu()
	{
		//add_options_page( 'Unité locale', 'Unité locale', 'manage_options', 'unitelocale', __CLASS__.'::options_page' );
		add_menu_page( 'Unité locale', 'Unité locale', 'manage_options', 'unitelocale', __CLASS__.'::options_page', get_stylesheet_directory_uri().'/images/settings.png');
	}

	/**
	 * Callback concernant la section
	 */
	static function section_callback()
	{
		//echo __( 'This section description', 'crf' );
	}


	/**
	 * Création de la page
	 */
	static function options_page()
	{
		?>
			<form action='options.php' method='post'>
				
				<h2>Informations concernant l'unité locale</h2>
				
				<?php
				settings_fields( 'unitelocale' );
				do_settings_sections( 'unitelocale' );
				submit_button();
				?>
				
			</form>
			<?php
		
		}
		
	/**
	 * Init : Ajouter la section, description, champs...
	 */
	static function init()
	{
		register_setting('unitelocale', 'crf_ul_settings');
	
		add_settings_section(
			'crf_ul_settings_section',
			'',//__( 'Your section description', 'crf' ),
			__CLASS__.'::section_callback',
			'unitelocale'
		);
		/*
		add_settings_field(
			'crf_preposition_nom_ul',
			__( 'Préposition', 'crf' ),
			__CLASS__.'::preposition_nom_ul_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_preposition_nom_ul')
		);
		*/
		add_settings_field(
			'crf_nom_ul',
			__( 'Unité locale', 'crf' ),
			__CLASS__.'::nom_ul_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_nom_ul')
		);
	
		add_settings_field(
			'crf_adresse_ul',
			__( 'Adresse postale du local', 'crf' ),
			__CLASS__.'::adresse_postale_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_adresse_ul')
		);
	
		add_settings_field(
			'crf_telephone_ul',
			__( 'Téléphone', 'crf' ),
			__CLASS__.'::telephone_ul_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_telephone_ul')
		);
	
		add_settings_field(
			'crf_mail_ul',
			__( 'Adresse e-mail de contact', 'crf' ),
			__CLASS__.'::mail_ul_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_mail_ul')
		);
	
		add_settings_field(
			'crf_mail_webmaster_ul',
			__( 'Adresse e-mail du webmaster', 'crf' ),
			__CLASS__.'::mail_webmaster_ul_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_mail_webmaster_ul')
		);
	
		/*
		add_settings_field(
			'crf_etablissements_ul',
			__( 'Etablissements', 'crf' ),
			__CLASS__.'::etablissement_ul_render',
			'unitelocale',
			'crf_ul_settings_section',
			array('label_for' => 'crf_etablissements_ul_0')
		);
		*/
	}
	
	/**
	 * Champ "Nom de l'UL"
	 */
	static function nom_ul_render()
	{
		$options = get_option( 'crf_ul_settings' );
		?>
		<select id="preposition_crf_nom_ul" name='crf_ul_settings[preposition_nom_ul]'>
			<option value="de" <?php echo $options['preposition_nom_ul']=='de' ? "selected=\"selected\"": ""?>>de</option>
			<option value="d'" <?php echo $options['preposition_nom_ul']=='d\'' ? "selected=\"selected\"": ""?>>d'</option>
			<option value="du" <?php echo $options['preposition_nom_ul']=='du' ? "selected=\"selected\"": ""?>>du</option>
			<option value="des"   <?php echo $options['preposition_nom_ul']=='des' ? "selected=\"selected\"": ""?>>des</option>
			<option value="de la" <?php echo $options['preposition_nom_ul']=='de la' ? "selected=\"selected\"": ""?>>de la</option>
			<option value="de l'" <?php echo $options['preposition_nom_ul']=='de l' ? "selected=\"selected\"": ""?>>de l'</option>
		</select>
		<input type='text' id="crf_nom_ul" name='crf_ul_settings[nom_ul]' placeholder="Nom de l'unité locale. Ex. &quot;Rueil-Malmaison&quot;" value='<?php echo $options['nom_ul']; ?>' size="32">
		<?php
	}
	
	/**
	 * Champ "Adresse postale"
	 */
	static function adresse_postale_render()
	{ 
		$options = get_option( 'crf_ul_settings' );
		?>
		<input type='text' id="crf_adresse_ul" name='crf_ul_settings[adresse]' placeholder="Adresse de l'unité locale" value='<?php echo $options['adresse']; ?>' size="41">
		<?php
	}
	
	/**
	 * Champ "téléphone"
	 */
	static function telephone_ul_render()
	{
		$options = get_option( 'crf_ul_settings' );
		?>
		<input type='tel' id="crf_telephone_ul" name='crf_ul_settings[telephone]' value='<?php echo $options['telephone']; ?>' placeholder="Téléphone de contact" size="16" maxlength="16">
		<?php
	}
	
	/**
	 * Champ "mail"
	 */
	static function mail_ul_render()
	{ 
		$options = get_option( 'crf_ul_settings' );
		?>
		<input type='email' id="crf_mail_ul" name='crf_ul_settings[mail]' placeholder="Mail de contact" value='<?php echo $options['mail']; ?>' size="41">
		<?php
	}

	/**
	 * Champ "mail du webmaster"
	 */
	static function mail_webmaster_ul_render()
	{
		$options = get_option( 'crf_ul_settings' );
		?>
			<input type='email' id="crf_mail_webmaster_ul" name='crf_ul_settings[mail_webmaster]' placeholder="Mail du webmaster" value='<?php echo $options['mail_webmaster']; ?>' size="41">
		<?php
	}
		
	
	static function pre_update($value, $old_value)
	{
		return array_merge($old_value, $value);
	}
	
} // END CLASS

add_action('pre_update_option_crf_ul_settings', 'CRF_Settings_UL::pre_update', 10, 2);
add_action( 'admin_menu', 'CRF_Settings_UL::add_admin_menu' );
add_action( 'admin_init', 'CRF_Settings_UL::init');

