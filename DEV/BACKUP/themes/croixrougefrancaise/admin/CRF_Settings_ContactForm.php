<?php

/**
 * Mise à jour des e-mail du formulaire de contact
 * @author fraoult
 * 
 */
class CRF_Settings_ContactForm
{
	
	
	/**
	 * Mise à jour la liste des emails
	 * @param  array $old_value
	 * @param  array $value
	 * @return array
	 */
	static function update_emails($old_value, $value)
	{
		global $wpdb;
		
		$options = get_option( ' crf_ul_settings' );
		$contact_list = $options['contacts'];
		$etablissement_list = $options['etablissements'];

		if ($contact_list === NULL)
		{
			$contact_list = array();
		}
		if ($etablissement_list === NULL)
		{
			$etablissement_list = array();
		}
		
		$mail_list = array();
		
		$webmaster_mail = crf_get_option('mail_webmaster');
		$ul_mail        = crf_get_option('mail');
				
		// Unité locale
		
		if (!empty($ul_mail))
		{
			$mail_list[] = '"Unité locale|'.$ul_mail.'"';
		}

		// Contacts
		
		foreach($contact_list as $type => $contact)
		{
			$mail = isset($contact['mail']) ? trim($contact['mail']) : '';
			if (! empty($mail))
			{
				$mail_list[] = '"'.CRF_Settings_UL_Contacts::$contact_list[$type].'|'.$mail.'"';
			}
		}
		
		// Etablissements
		
		foreach($etablissement_list as $etablissement)
		{
			$type = $etablissement['type'];
			$mail = $etablissement['mail'];
			if (! empty($mail))
			{
				$mail_list[] = '"'.CRF_Settings_UL_Etablissements::$etablissements_list[$type].'|'.$mail.'"';
			}
		}
		
		// Webmaster
		
		if (!empty($webmaster_mail))
		{
			$mail_list[] = '"Webmaster|'.$webmaster_mail.'"';
		}
		
		// Modification des destinataires

		$post_list = get_posts(array('post_type' => 'wpcf7_contact_form'));
		if (empty($post_list))
		{
			return;
		}
		$post = reset($post_list);
		$contact_form = wpcf7_contact_form($post->ID);
		if ( empty( $contact_form ) ) {
			$contact_form = WPCF7_ContactForm::get_template();
		}
		
		$properties = $contact_form->get_properties();
		$form       = $properties['form'];
		$form       = preg_replace('`\[select\* destinataire-crf\s.*?\]`ims', '[select* destinataire-crf'.PHP_EOL.join(' '.PHP_EOL, $mail_list).']', $form);
		$properties['form'] = $form;
		$contact_form->set_properties( $properties );
		$contact_form->save();
		return $value;
	}
	
} // END CLASS

add_action('update_option_crf_ul_settings', 'CRF_Settings_ContactForm::update_emails', 10, 2);
