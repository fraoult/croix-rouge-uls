<?php 

include_once 'CRF_Admin_Filters.php';
include_once 'CRF_Settings_UL.php';
include_once 'CRF_Settings_UL_Etablissements.php';
include_once 'CRF_Settings_UL_Contacts.php';
include_once 'CRF_Settings_Map_Etablissement.php';
include_once 'CRF_Settings_ContactForm.php';

include_once 'options/00.variables.php';
include_once 'options/02.homepage.php';

function crf_admin_color_palette() {
	wp_admin_css_color(
		'crf-colors',
		__('Croix-Rouge Française'),
		get_stylesheet_directory_uri() . '/css/colors/crf/colors.css',
		array('#FF0000', '#D10049', '#69063A', '#FF5912', '#222E07', '#586616', '#D9C883', '#B28D47', '#295669', '#6A95A3', '#000000', '#404040', '#8C8C8C', '#FFFFFF'),
		array('current' => '#6A95A3', 'focus' => '#D10049', 'base' => '#404040')
	);
}
add_action('admin_init', 'crf_admin_color_palette');

