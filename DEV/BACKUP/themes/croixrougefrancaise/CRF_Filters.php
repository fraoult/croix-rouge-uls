<?php

/**
 * Les filtres
 * @author fraoult
 * 
 */
class CRF_Filters
{

		
	/**
	 * Le nom de l'UL devant le titre
	 * @param unknown $value
	 * @return string
	 */
	function wp_title($title)
	{
		return 'Unité locale '.CRF::get_option('nom_ul_avec_preposition').' | '.$title;
	}
	
	
	/**
	 * Variable de configuration dans les textes type {$UL_nom_du_champ_de_config}
	 * @param unknown $content
	 * @return mixed
	*/
	function the_content($content)
	{
		return preg_replace_callback('`{\$UL_([a-zA-Z0-9_]+)}`', create_function('$matches', 'return crf_get_option($matches[1]);'), $content);
	}
	
	/**
	 * Entourer le contenu d'un article par une section
	 * @param string $content
	 * @return string
	 */
	function wrap_content($content)
	{
		return '<section data-type="main-content">'.PHP_EOL.$content.PHP_EOL.'</section>';
	}
	
} // END CLASS

add_filter('wp_title', 'CRF_Filters::wp_title');
add_filter('the_content', 'CRF_Filters::the_content');
add_filter('the_content', 'CRF_Filters::wrap_content', 1, 1);
add_filter('the_excerpt', 'CRF_Filters::wrap_content', 1, 1);
