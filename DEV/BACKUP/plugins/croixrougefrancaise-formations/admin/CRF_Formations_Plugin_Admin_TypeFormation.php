<?php
/**
 * Les formations
 * @author fraoult
 *
 */
class CRF_Formations_Plugin_Admin_TypeFormation
{

	/**
	 * Ajouter le lien au menu
	 */
	static function add_admin_menu()
	{
		add_submenu_page('unitelocale_formations', 'Types de formations', 'Types de formations', 'manage_options', 'unitelocale_formation_types', __CLASS__.'::options_page' );
	}

	/**
	 * Callback concernant la section
	 */
	static function section_callback()
	{
		//echo __( 'This section description', 'crf' );
	}


	/**
	 * Création de la page
	 */
	static function options_page()
	{
		?>
			<form action='options.php' method='post'>
				
				<h2>Formations</h2>
				<p>Indiquer vos dates de formations !</p>
				<?php
				settings_fields( 'unitelocale_formation_types' );
				do_settings_sections( 'unitelocale_formation_types' );
				submit_button();
				?>
				
			</form>
		<?php
	}
		
	/**
	 * Init : Ajouter la section, description, champs...
	 */
	static function init()
	{
		register_setting('unitelocale_formation_types', 'crf_formations_types');
	
		add_settings_section(
			'crf_formation_type_section',
			'',//__( 'Your section description', 'crf' ),
			__CLASS__.'::section_callback',
			'unitelocale_formation_types'
		);
		
		add_settings_field(
			'crf_formation_types',
			__( 'Types de formations', 'crf' ),
			__CLASS__.'::formation_types_render',
			'unitelocale_formation_types',
			'crf_formation_type_section',
			array('label_for' => 'crf_formation_types_0')
		);
		
	}
		
	/**
	 * Render des formations
	 */
	static function formation_types_render()
	{
		global $wpdb;
		
		$type_formations_list = $wpdb->get_results('select * from '.$wpdb->prefix.'crf_formations_type where obsolete=0', ARRAY_A);
		?>
		<?php foreach($type_formations_list as $i => $formation_type) : ?>
			<section data-type="formations" data-index="<?php echo $formation_type['id']?>">
				<input type="text" name="crf_formation_type[<?php echo $formation_type['id']?>][nom]" placeholder="Nom court" value="<?php echo $formation_type['nom'] ?>" />
				<textarea name="crf_formation_type[<?php echo $formation_type['id']?>][description]" placeholder="Description longue" value="<?php echo $formation_type['description'] ?>"></textarea>
				<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][grand_public]" placeholder="Formation grand public ?" <?php if ($formation_type['grand_public']) echo 'checked="checked"' ?> />
				<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][obsolete]" placeholder="Formation obsolète ?" <?php if ($formation_type['obsolete']) echo 'checked="checked"' ?> />
			</section>
		<?php endforeach; ?>
		<?php  if (empty($type_formations_list)) :?>
			<section data-type="formations" data-index="0">
				<input type="text" name="crf_formation_type[<?php echo $formation_type['id']?>][nom]" placeholder="Nom court" />
				<textarea name="crf_formation_type[<?php echo $formation_type['id']?>][description]" placeholder="Description longue"></textarea>
				<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][grand_public]" placeholder="Formation grand public ?" checked="checked"/>
				<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][obsolete]" placeholder="Formation obsolète ?"  />
				<button type="button" data-type="suppr_formation" data-ref="<?php echo $i?>">Supprimer</button>
			</section>
		<?php endif; ?>
		<button type="button" id="crf_formation_ajouter">Ajouter un type de formation</button>
		<?php 
	}
			
	
	protected static function get_formation_type_select($type_formations_list, $index, $selected_value=NULL)
	{
		global $wpdb;
		$formations_grand_public_list = array();
		$formations_internes_list = array();
		foreach($type_formations_list as $type_formation)
		{
			$id = $type_formation['id'];
			if ($type_formation['grand_public'])
			{
				$formations_grand_public_list[$id] = $type_formation;		
			}
			else
			{
				$formations_internes_list[$id] = $type_formation;
			}
		}
		?>
			<select id="crf_formations_<?php echo $index?>" name="crf_formations[<?php echo $index?>][type]" required="required" data-type="formations" />
				<optgroup label="Formations grand public">
				<?php foreach($formations_grand_public_list as $id => $formation) :?>
					<option value="<?php echo $id ?>" data-description="<?php echo $formation['description']?>"<?if($selected_value==$id) echo ' selected="selected"'?>><?php echo $formation['nom']?></option>
				<?php endforeach;?>
				</optgroup>
				<optgroup label="Formations de bénévoles">
				<?php foreach($formations_internes_list as $id => $formation) :?>
					<option value="<?php echo $id ?>" data-description="<?php echo $formation['description']?>"<?if($selected_value==$id) echo ' selected="selected"'?>><?php echo $formation['nom']?></option>
				<?php endforeach;?>
				</optgroup>
			</select>
		<?php
	}
			
			
} // END CLASS


add_action( 'admin_menu', 'CRF_Formations_Plugin_Admin_TypeFormation::add_admin_menu', 11);
add_action( 'admin_init', 'CRF_Formations_Plugin_Admin_TypeFormation::init', 11);
