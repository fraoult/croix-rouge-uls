<?php
/**
 * Les formations
 * @author fraoult
 *
 */
class CRF_Formations_Plugin_Admin
{

	/**
	 * Ajouter le lien au menu
	 */
	static function add_admin_menu()
	{
		add_submenu_page('unitelocale', 'Formations', 'Formations', 'manage_options', 'unitelocale_formations', __CLASS__.'::options_page' );
	}

	/**
	 * Callback concernant la section
	 */
	static function section_callback()
	{
		//echo __( 'This section description', 'crf' );
	}


	/**
	 * Création de la page
	 */
	static function options_page()
	{
		$tab_type = isset($_GET['tab']) && $_GET['tab'] == 'type' ? ' nav-tab-active' : '';
		$tab_date = ! isset($_GET['tab']) || $_GET['tab'] == 'date' ? ' nav-tab-active' : '';
		?>
			<form action='options.php' method='post'>
				
				<h2>Formations</h2>
				<div id="icon-themes" class="icon32" id="icon-options-general"></div>
				<h2 class="nav-tab-wrapper">
					<a href="<?php menu_page_url('unitelocale_formations')?>&tab=date" class="nav-tab<?php echo $tab_date?>">Les dates</a>
					<a href="<?php menu_page_url('unitelocale_formations')?>&tab=type" class="nav-tab<?php echo $tab_type?>">Les formations</a>
				</h2>
				<?php	
			    switch ($_GET['tab']) {
			    	default:
			        case 'date':
			        	echo '<p>Indiquez vos dates de formation à venir</p>';
			        	self::formations_render();
						//settings_fields( 'unitelocale_formations' );
						//do_settings_sections( 'unitelocale_formations' );
						break;
			        case 'type':
			        	echo '<p>Liste de formations dispensées</p>';
			        	self::formation_types_render();
			            break;
			    }
				submit_button();
			    ?>
				
			</form>
		<?php
	}
		
	/**
	 * Init : Ajouter la section, description, champs...
	 */
	static function init()
	{
		/*
		register_setting('unitelocale_formations', 'crf_formations');
	
		add_settings_section(
			'crf_formation_section',
			'',//__( 'Your section description', 'crf' ),
			__CLASS__.'::section_callback',
			'unitelocale_formations'
		);
		
		add_settings_field(
			'crf_formations',
			__( 'Formations', 'crf' ),
			__CLASS__.'::formations_render',
			'unitelocale_formations',
			'crf_formation_section',
			array('label_for' => 'crf_formations_0')
		);
		*/
	}
		
	
	/**
	 * Render des formations
	 */
	static function formations_render()
	{
		global $wpdb;
		
		$formations_list      = $wpdb->get_results('select * from '.$wpdb->prefix.'crf_formations where date_debut >= NOW() - INTERVAL 1 MONTH;');
		$type_formations_list = $wpdb->get_results('select * from '.$wpdb->prefix.'crf_formations_type where obsolete=0', ARRAY_A);
		?>
		<table class="wp-list-table widefat fixed striped">
			<thead>
				<tr>
					<th scope="col" class="manage-column">Formation</th>
					<th scope="col" class="manage-column">Début</th>
					<th scope="col" class="manage-column">Fin</th>
					<th scope="col" class="manage-column">Détail</th>
					<th scope="col" class="manage-column"></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($formations_list as $i => $formation) : ?>
				<tr data-type="formations" data-index="<?php echo $i?>">
					<td>
						<?php echo $formation['type'] ?>
						<?php // self::get_formation_type_select($type_formations_list, $i, $formation['type']); ?>
					</td>
					<td>
						<time><?php echo $formation['date_debut'] ?></time>
						<!--
						<input type="date" name="crf_formations[<?php echo $i?>][date_debut][date]" placeholder="Début" value="<?php echo $formation['date_debut'] ?>" />
						<input type="time" name="crf_formations[<?php echo $i?>][date_debut][time]" placeholder="Début" value="<?php echo $formation['date_debut'] ?>" />
						 -->
					</td>
					<td>
						<time><?php echo $formation['date_fin'] ?></time>
						<!--
						<input type="date" name="crf_formations[<?php echo $i?>][date_fin][date]" placeholder="Fin" value="<?php echo $formation['date_fin'] ?>" />
						<input type="time" name="crf_formations[<?php echo $i?>][date_fin][time]" placeholder="Fin" value="<?php echo $formation['date_fin'] ?>" />
						 -->
					</td>
					<td>
						<?php echo $formation['detail'] ?>
						<!--
						<textarea name="crf_formations[<?php echo $i?>][detail]" placeholder="Détail de la formation (détail des horaires si plusieurs jours, etc.)">"<?php echo $formation['detail'] ?></textarea>
						 -->
					</td>
					<td>
						<button type="button" data-type="suppr_formation" data-ref="<?php echo $i?>">Supprimer</button>
					</td>
				</tr>
			<?php endforeach; ?>
			<?php  if (empty($formations_list)) :?>
				<tr>
					<td colspan="5" style="text-align: center; font-variant: small-caps; font-size: large;"><strong>- Aucune formation existante à venir -</strong></td>
				</tr>
			<?php endif;?>
			</tbody>
			<tfoot>
				<tr>
					<th scope="col" class="manage-column">Formation</th>
					<th scope="col" class="manage-column">Début</th>
					<th scope="col" class="manage-column">Fin</th>
					<th scope="col" class="manage-column">Détail</th>
					<th scope="col" class="manage-column"></th>
				</tr>
			<?php  if (empty($formations_list)) :?>
				<tr data-type="formations" data-index="0">
					<td>
						<?php self::get_formation_type_select($type_formations_list, '0'); ?>
					</td>
					<td>
						<input type="date" name="crf_formations[0][date_debut][date]" placeholder="Début" />
						<input type="time" name="crf_formations[0][date_debut][time]" placeholder="Début" />
					</td>
					<td>
						<input type="date" name="crf_formations[0][date_fin][date]" placeholder="Fin" />
						<input type="time" name="crf_formations[0][date_fin][time]" placeholder="Fin" />
					</td>
					<td>
						<textarea name="detail" placeholder="Détails éventuels (horaires détaillés, etc.)" style="width: 100%; height: 7em;"></textarea>
					</td>
					<td>
						<button type="button" data-type="suppr_formation" data-ref="0">Ajouter</button>
					</td>
				</tr>
			<?php endif; ?>
			</tfoot>
		</table>
		<button type="button" id="crf_formation_ajouter">Ajouter une date de formation</button>
		<?php 
	}
			
	
	protected static function get_formation_type_select($type_formations_list, $index, $selected_id=NULL)
	{
		global $wpdb;
		$formations_grand_public_list = array();
		$formations_internes_list = array();
		foreach($type_formations_list as $type_formation)
		{
			$id = $type_formation['id'];
			if ($type_formation['grand_public'])
			{
				$formations_grand_public_list[$id] = $type_formation;		
			}
			else
			{
				$formations_internes_list[$id] = $type_formation;
			}
		}
		?>
			<select id="crf_formations_<?php echo $index?>" name="crf_formations[<?php echo $index?>][type]" required="required" data-type="formations" />
				<optgroup label="Formations grand public">
				<?php foreach($formations_grand_public_list as $id => $formation) :?>
					<option value="<?php echo $id ?>" data-description="<?php echo $formation['description']?>"<?if($selected_id==$id) echo ' selected="selected"'?>><?php echo $formation['nom']?></option>
				<?php endforeach;?>
				</optgroup>
				<optgroup label="Formations de bénévoles">
				<?php foreach($formations_internes_list as $id => $formation) :?>
					<option value="<?php echo $id ?>" data-description="<?php echo $formation['description']?>"<?if($selected_id==$id) echo ' selected="selected"'?>><?php echo $formation['nom']?></option>
				<?php endforeach;?>
				</optgroup>
			</select>
		<?php
	}
			
	
	/**
	 * Render des formations
	 */
	static function formation_types_render()
	{
		global $wpdb;
	
		$type_formations_list = $wpdb->get_results('select * from '.$wpdb->prefix.'crf_formations_type', ARRAY_A);
		?>
		<table class="wp-list-table widefat fixed striped">
			<thead>
				<tr>
					<th scope="col" class="manage-column">Nom court / Accronyme</th>
					<th scope="col" class="manage-column">Nom long</th>
					<th scope="col" class="manage-column">Description affichée</th>
					<th scope="col" class="manage-column">Grand public ?</th>
					<th scope="col" class="manage-column">Obsolète ?</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($type_formations_list as $i => $formation_type) : ?>
				<tr data-type="formations" data-index="<?php echo $formation_type['id']?>">
					<td>
						<?php echo $formation_type['nom'] ?>
						<!-- 
						<input type="text" name="crf_formation_type[<?php echo $formation_type['id']?>][nom]" placeholder="Nom court" value="<?php echo $formation_type['nom'] ?>" size="10" />
						-->
					</td>
					<td>
						<?php echo $formation_type['nom_long'] ?>
						<!--
						<textarea name="crf_formation_type[<?php echo $formation_type['id']?>][description]" placeholder="Nom complet" style="width: 100%; height: 7em;" ><?php echo $formation_type['description'] ?></textarea>
						-->
					</td>
					<td>
						<?php echo $formation_type['description'] ?>
						<!--
						<textarea name="crf_formation_type[<?php echo $formation_type['id']?>][description]" placeholder="Description affichée (tarifs, durée, etc.)" style="width: 100%; height: 7em;" ><?php echo $formation_type['description'] ?></textarea>
						-->
					</td>
					<td>
						<?php echo ($formation_type['grand_public']) ? 'OUI' : 'NON' ?>
						<!--
						<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][grand_public]" placeholder="Formation grand public ?" <?php if ($formation_type['grand_public']) echo 'checked="checked"' ?> />
						-->
					</td>
					<td>
						<?php echo ($formation_type['obsolete']) ? 'OUI' : 'NON' ?>
						<!--
						<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][obsolete]" placeholder="Formation obsolète ?" <?php if ($formation_type['obsolete']) echo 'checked="checked"' ?> />
						-->
					</td>
				</tr>
			<?php endforeach; ?>
			<?php  if (empty($type_formations_list)) :?>
				<tr>
					<td colspan="5" style="text-align: center; font-variant: small-caps; font-size: large;"><strong>- Aucun type de formation existant -</strong></td>
				</tr>
			<?php endif;?>
			</tbody>
			<tfoot>
				<tr>
					<th scope="col" class="manage-column">Nom court / Accronyme</th>
					<th scope="col" class="manage-column">Nom long</th>
					<th scope="col" class="manage-column">Description affichée</th>
					<th scope="col" class="manage-column">Grand public ?</th>
					<th scope="col" class="manage-column">Obsolète ?</th>
				</tr>
			<?php  if (empty($type_formations_list)) :?>
				<tr data-type="formations" data-index="0">
					<td>
						<input type="text" name="crf_formation_type[<?php echo $formation_type['id']?>][nom]" placeholder="Nom court" size="10" />
					</td>
					<td>
						<input type="text" name="crf_formation_type[<?php echo $formation_type['id']?>][nom_long]" placeholder="Nom complet" />
					</td>
					<td>
						<textarea name="crf_formation_type[<?php echo $formation_type['id']?>][description]" placeholder="Description affichée (tarifs, durée, etc.)" style="width: 100%; height: 7em;"></textarea>
					</td>
					<td>
						<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][grand_public]" placeholder="Formation grand public ?" checked="checked"/>
					</td>
					<td>
						<input type="checkbox" name="crf_formation_type[<?php echo $formation_type['id']?>][obsolete]" placeholder="Formation obsolète ?"  />
					</td>
				</tr>
			<?php endif; ?>
			</tfoot>
		</table>
		<button type="button" id="crf_formation_type_ajouter">Ajouter un type de formation</button>
		<?php 
	}
		
			
} // END CLASS


add_action( 'admin_menu', 'CRF_Formations_Plugin_Admin::add_admin_menu', 11);
add_action( 'admin_init', 'CRF_Formations_Plugin_Admin::init', 11);
