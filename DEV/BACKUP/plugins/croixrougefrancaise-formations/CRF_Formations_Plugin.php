<?php
/**
* Entrée principale du plugin
*/

/**
 * Classe principale du plugin
 * @author Francois RAOULT, Croix-Rouge Française, Délégation départementale des Hauts-de-Seine
 */
class CRF_Formations_Plugin
{

	/**
	 * Lancement du plugin
	 */
	public function __construct()
	{
		include_once 'admin/CRF_Formations_Plugin_Admin.php';
		include_once 'admin/CRF_Formations_Plugin_Admin_TypeFormation.php';
	}
	

	
	/**
	 * Installation du plugin
	 */
	public static function install()
	{
		global $wpdb;
		
		$result = $wpdb->query('CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'crf_formations_type (
			id INT AUTO_INCREMENT,
			nom TINYTEXT,
			nom_long TINYTEXT,
			description TEXT,
			grand_public TINYINT(1) NOT NULL DEFAULT 1,
			obsolete TINYINT(1) NOT NULL DEFAULT 0,
			PRIMARY KEY(id)
		);');
		
		if (!$result)
		{
			die ($wpdb->last_error);
		}
		
		$id = $wpdb->get_var('SELECT id FROM '.$wpdb->prefix.'crf_formations_type LIMIT 1', 0, 0);
		if (empty($id))
		{
			$wpdb->query('INSERT INTO '.$wpdb->prefix.'crf_formations_type (nom, nom_long, description, grand_public) VALUES("PSC1 - IRR", "Prévention et secours civiques de niveau 1 et Initiation à la réduction des risques", "Apprendre à sauver des vies en quelques heures", 1);');
			$wpdb->query('INSERT INTO '.$wpdb->prefix.'crf_formations_type (nom, nom_long, description, grand_public) VALUES("IPS", "Initiation aux premiers secours", "Se former en 1h aux gestes qui sauvent", 1);');
			$wpdb->query('INSERT INTO '.$wpdb->prefix.'crf_formations_type (nom, nom_long, description, grand_public) VALUES("PSE1", "Premiers secours en équipe de niveau 1", "", 0);');
			$wpdb->query('INSERT INTO '.$wpdb->prefix.'crf_formations_type (nom, nom_long, description, grand_public) VALUES("PSE2", "Premiers secours en équipe de niveau 2", "" , 0);');
			$wpdb->query('INSERT INTO '.$wpdb->prefix.'crf_formations_type (nom, nom_long, description, grand_public) VALUES("PSE", "Premiers secours en équipe (niveaux 1 + 2)", "", 0);');
			$wpdb->query('INSERT INTO '.$wpdb->prefix.'crf_formations_type (nom, nom_long, description, grand_public) VALUES("OPR", "Opérateur radio", "Apprendre les procédure nécessaires pour utiliser les outils de radiocommunication à la Croix-Rouge", 0);');
		}
		
		$result = $wpdb->query('CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'crf_formations (
			id INT AUTO_INCREMENT,
			type INT NULL DEFAULT NULL,
			date_debut TIMESTAMP NOT NULL,
			date_fin   TIMESTAMP NOT NULL,
			detail     TEXT,
			PRIMARY KEY(id),
			INDEX(type),
			FOREIGN KEY (type) REFERENCES '.$wpdb->prefix.'crf_formations_type(id) ON UPDATE CASCADE ON DELETE SET NULL
		);');
		
		if (!$result)
		{
			die ($wpdb->last_error);
		}
	}

	/**
	 * Suppression totale du plugin
	 */
	public static function uninstall()
	{
		global $wpdb;
		$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}crf_formations;");
		$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}crf_formations_type;");
	}
}


// Démarrer le plugin !

register_activation_hook(__FILE__, array('CRF_Formations_Plugin', 'install'));
register_uninstall_hook(__FILE__,  array('CRF_Formations_Plugin', 'uninstall'));
new CRF_Formations_Plugin();

