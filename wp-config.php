<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
//define('DB_NAME', 'u207924871_perso');
define('DB_NAME', 'crf_ul92');

/** Utilisateur de la base de données MySQL. */
//define('DB_USER', 'u207924871_perso');
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
//define('DB_PASSWORD', 'neaphowe84');
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5:m+}Mmx-?5ujV&5Hg65:d>_H|ZB7OAJ>vq(<kW^(}UhfDehK hMTzh36ZN!2KhY');
define('SECURE_AUTH_KEY',  ')z]W!m<#1u]L?RmBmOZQl0[9;ycaRJS|$X0egW_a)nDf,vz~:zpbU_o{Cw k[GI>');
define('LOGGED_IN_KEY',    'O:=noDHBK,-4HD&~Zh*HUP:zOZGV<xBD~aOO9x3DI/N%O6lH}ze)nl!d1T_ ow%c');
define('NONCE_KEY',        '/|=%6%bO<lo,@w^_Kdm6/5*<AR,.8AEMK!iH^dZJ1QXFkpg-`/b{BIalCzG{7M[f');
define('AUTH_SALT',        'pAkAc<%B 0>eX-^Tw>wBcK`JJ0?sWJQ=-,{n~(rj=?XrIV)Z9^Ly|KJ!=:ivpKj3');
define('SECURE_AUTH_SALT', '[ps@^a3%(|}](vu;0Yba@6|N>Q&d>4U<WeG`X||U7yi?|ZpQE<1$|xl0SYy=<(LU');
define('LOGGED_IN_SALT',   '|3Ak^!ivOlZ*=)ZC(v+E_:@dA-e!:!5+.{lT**HiGD7 -F|%*7MrpptU$[wZ-@[i');
define('NONCE_SALT',       'tO?o`-3L9.O,lWYWx,>IFX_i9[v!FCz-qDM%wB-fi+S^O)vE:p1f]^k0x` D}#`&');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Adresse WordPress (URL)
 * WP_SITEURL, définie depuis WordPress Version 2.2, autorise la définition de l'adresse (URL) de WordPress.
 * La valeur définie correspond à l'adresse où vos fichiers de base de WordPress résident.
 * Il doit inclure également le http://. N'ajoutez pas de "/" à la fin.
 * Définir cette valeur dans wp-config.php se substitue à la valeur siteurl dans la table wp_options
 * et désactive le champ 'Adresse web de WordPress (URL)' dans le Tableau de bord > Réglages > Général.
 * NOTE: Cela ne changera pas la valeur dans la base de données et l'url reviendra à son ancienne valeur
 * dans la base de données si cette ligne est retirée de wp-config.php.
 * Utilisez la constante RELOCATE pour changer la valeur 'siteurl' dans la base de données.
 */

// define('WP_SITEURL', 'http://'.$_SERVER['HTTP_HOST'].'/chemin/vers/wordpress');

/**
 * Adresse du Blog (URL)
 * WP_HOME est une autre option du wp-config.php ajoutées dans WordPress Version 2.2.
 * Similaire à WP_SITEURL, WP_HOME se substitue à la valeur home dans la table wp_options dans la base de données,
 * mais ne la change pas de manière permanente..
 * C'est l'adresse que vous souhaitez voir tapée dans le navigateur par les utilisateurs pour accéder à votre site.
 * Il doit inclure également le http:// et ne pas avoir de slash "/" à la fin.
 */

// define('WP_HOME', 'http://'.$_SERVER['HTTP_HOST'].'/chemin/vers/wordpress');

/**
 * Déplacer les répertoires wp-content, de plugin, d'upload
 * Depuis la Version 2.6, vous pouvez déplacer le répertoire wp-content, qui contient vos thèmes,
 * vos extensions et téléchargements, à l'extérieur du répertoire applicatif de WorddPress.
 * 
 * Les constantes _DIR doivent indiquer le chemin local complet du répertoire (sans slash à la fin)
 * Par exemple : $_SERVER['DOCUMENT_ROOT'] . '/blog/wp-content
 * Les constantes _URL doivent indiquer l'URL complète du répertoire (sans slash à la fin)
 * Par exemple : http://example/blog/wp-content
 */

// define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/blog/wp-content');
// define( 'WP_CONTENT_URL', 'http://example/blog/wp-content');
// define( 'WP_PLUGIN_DIR', $_SERVER['DOCUMENT_ROOT'] . '/blog/wp-content/plugins' );
// define( 'WP_PLUGIN_URL', 'http://example/blog/wp-content/plugins');

// define( 'UPLOADS', '/blog/wp-content/uploads' );


/**
 * Désactiver la révision des articles ; spécifier un nombre maximum de révisions
 */

// define('WP_POST_REVISIONS', false ); // true, false ou un nombre. (true par défaut)

/**
 * Définir les Cookies de Domaine
 * Le domaine défini dans les cookies pour WordPress peut être spécifiée pour les personnes
 * ayant des configurations inhabituelles de domaine. Une des raisons est que si les sous-domaines
 * sont utilisés pour servir du contenu statique (en anglais).
 * Pour éviter aux cookies WordPress d'être envoyé avec chaque requête au contenu statique sur
 * votre sous-domaine, vous pouvez définir le domaine de cookie seulement pour votre domaine non-statique.
 */

// define('COOKIE_DOMAIN', 'www.askapache.com');

/**
 * Activer le Multisite / Network Ability
 * WP_ALLOW_MULTISITE est une fonctionnalité introduite dans WordPress Version 3.0
 * pour activer les fonctionnalités multisites (en anglais) fonctionnant auparavant avec WordPress MU.
 * Si ce réglage est absent du wp-config.php sa valeur est par défaut false.
 */

// define('WP_ALLOW_MULTISITE', true);


/**
 * Options statiques
 * De la même façon que WP_HOME et WP_SITEURL, les options statiques permettent de forcer les valeurs
 * des options indiquées
 */

define('OPTION_BLOGNAME', 'Croix-Rouge Française');
define('OPTION_BLOGDESCRIPTION', 'Sans vous, comment ferions-nous ?');
// define('OPTION_DATEFORMAT', 'j F Y');
// define('OPTION_TIMEFORMAT', 'H:i:s');
// define('OPTION_TIMEZONE', '');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
