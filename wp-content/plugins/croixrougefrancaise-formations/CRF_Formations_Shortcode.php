<?php

/**
 * Les filtres
 * @author fraoult
 * 
 */
class CRF_Formations_Shortcode
{

	public static function show_formations($atts, $contents=null, $code='')
	{
		$type_id = NULL;
		$type_name = NULL;
		
		if (isset($atts['type_id']))
		{
			$type_id = $atts['type_id'];
		}
		elseif (isset($atts['type_name']))
		{
			$type_name = $atts['type_name'];
		}
		elseif (isset($atts['type']))
		{
			if (is_int($atts['type']))
			{
				$type_id = (int)$atts['type'];
			}
			else
			{
				$type_name = (string)$atts['type'];
			}				
		}
		
		global $wpdb;
		
		$query = '
			select
				t.nom,
				f.id as id,
				f.date_debut as date_debut,
				f.date_fin as date_fin,
				f.detail
			from '.$wpdb->prefix.'crf_formations f
			join '.$wpdb->prefix.'crf_formations_type t on f.type = t.id
			where
				date_debut >= NOW()
				and t.obsolete = 0
			';
		$args  = array();
		if (!empty($type_id))
		{
			$query .= ' and t.id = %d';
			$args[] = $type_id;
		}
		elseif(!empty($type_name))
		{
			$query .= ' and t.nom = %s ';
			$args[] = $type_name;
		}
		
		if (array_key_exists('grand_public', $atts))
		{
			$query .= ' and t.grand_public = %d';
			$args[] = (int)(boolean)$atts['grand_public'];
		}

		$query .= ' order by f.date_debut asc';
		
		$row_list = $wpdb->get_results($wpdb->prepare($query, $args), ARRAY_A);

		$query = 'select id, nom, nom_long, description from '.$wpdb->prefix.'crf_formations_type where id = %d or nom = %s';
		$type  = $wpdb->get_row($wpdb->prepare($query, array($type_id, $type_name)), ARRAY_A, 0);
		
		$date_format = str_replace('Y', '', get_option('date_format', 'j F'));
		$time_format = get_option('time_format', 'H:i');
		$dt_format   = $date_format.' à '.$time_format;
		
		$row = reset($row_list);
		
		$result = '';
		
		$result .= '<section class="crf-formation '.($type['grand_public'] ? 'grandpublic' : 'interne').'" data-type-nom="'.$type['nom'].'" data-type-id="'.$type['id'].'">';
		$result .= '<hgroup>';
		$result .= '<h2>'.$type['nom'].'</h2>';
		$result .= '<h3>'.$type['nom_long'].'</h3>';
		$result .= '</hgroup>';
		$result .= '<p class="crf-formation-description">'.$type['description'].'</p>'; 
		if (empty($row_list))
		{
			return $result.'<p><strong>Aucune formation '.$type['nom'].' prévue</strong></p>';
		}
		
		$result .= '<ul class="crf-formation-liste-dates">';
		foreach($row_list as $row)
		{
			$result .= '<li>';
			$result .= '<header>'.date_i18n($dt_format, strtotime($row['date_debut'])).' au '.date_i18n($dt_format, strtotime($row['date_fin'])).'</header>';
			$result .= '<p>'.$row['detail'].'</p>';
			$result .= '</li>';
		}
		$result .= '</ul>';
		$result .= '</section>';
		
		echo mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result));
		
	}
	
		
	
} // END CLASS

add_shortcode('crf-formations', 'CRF_Formations_Shortcode::show_formations');
