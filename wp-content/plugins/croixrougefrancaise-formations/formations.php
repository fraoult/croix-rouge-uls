<?php
/**
* Plugin Name: Croix-Rouge Française, Formations
* Description: Permet d'indiquer les dates de vos formations grand-public (PSC1, etc.)
* Version: 0.1
* Author: François RAOULT, Croix-Rouge Française, Délégation départementale des Hauts-de-Seine
* Author URI: http://92.croix-rouge.fr  
*/

include_once 'CRF_Formations.php';

register_activation_hook(__FILE__, array('CRF_Formations_Plugin', 'install'));

new CRF_Formations();

