<?php
/**
 * Les formations
 * @author fraoult
 *
 */
class CRF_Formations_Plugin_Admin
{
	
	private static $_item = NULL;

	/**
	 * Ajouter le lien au menu
	 */
	static function add_admin_menu()
	{
		add_submenu_page('unitelocale', 'Formations', 'Formations', 'manage_options', 'unitelocale_formations', __CLASS__.'::options_page' );
	}

		
	// -------------------------------------------------------------------------------------
	// INIT
	// -------------------------------------------------------------------------------------
	
	/**
	 * Init : Ajouter la section, description, champs...
	 */
	static function init()
	{
		self::init_form_date();
		self::init_form_type();
	}
	
	/**
	 * Initialisation des champs pour un type de formation
	 */
	static function init_form_type()
	{
		add_settings_section(
			'crf_formation_add_type_section',
			'',//__( 'Your section description', 'crf' ),
			'', //__CLASS__.'::section_add_type_callback',
			'unitelocale_formations'
		);
		
		add_settings_field(
			'nom',
			__( 'Nom court', 'crf' ),
			__CLASS__.'::nom_render',
			'unitelocale_formations',
			'crf_formation_add_type_section',
			array('label_for' => 'nom')
		);
		
		add_settings_field(
			'nom_long',
			__( 'Nom long', 'crf' ),
			__CLASS__.'::nom_long_render',
			'unitelocale_formations',
			'crf_formation_add_type_section',
			array('label_for' => 'nom_long')
		);
		
		add_settings_field(
			'description',
			__( 'Description détaillée', 'crf' ),
			__CLASS__.'::description_render',
			'unitelocale_formations',
			'crf_formation_add_type_section',
			array('label_for' => 'description')
		);
		
		add_settings_field(
			'grand_public',
			__( 'Grand public ?', 'crf' ),
			__CLASS__.'::grand_public_render',
			'unitelocale_formations',
			'crf_formation_add_type_section',
			array('label_for' => 'grand_public')
		);

		add_settings_field(
			'obsolete',
			__( 'Obsolète ?', 'crf' ),
			__CLASS__.'::obsolete_render',
			'unitelocale_formations',
			'crf_formation_add_type_section',
			array('label_for' => 'obsolete')
		);
		
	}
	
	/**
	 * Initialisation des champs pour une date de formation
	 */
	static function init_form_date()
	{
	
		add_settings_section(
			'crf_formation_add_date_section',
			'',//__( 'Your section description', 'crf' ),
			'',//__CLASS__.'::section_add_date_callback',
			'unitelocale_formations'
		);
	
		add_settings_field(
			'type_formation',
			__( 'Type de formation', 'crf' ),
			__CLASS__.'::type_formation_render',
			'unitelocale_formations',
			'crf_formation_add_date_section',
			array('label_for' => 'type_formation')
		);
	
		add_settings_field(
			'date_debut',
			__( 'Date de début', 'crf' ),
			__CLASS__.'::date_debut_render',
			'unitelocale_formations',
			'crf_formation_add_date_section',
			array('label_for' => 'date_debut')
		);
	
		add_settings_field(
			'date_fin',
			__( 'Date de fin', 'crf' ),
			__CLASS__.'::date_fin_render',
			'unitelocale_formations',
			'crf_formation_add_date_section',
			array('label_for' => 'date_fin')
		);
	
		add_settings_field(
			'detail',
			__( 'Détails', 'crf' ),
			__CLASS__.'::detail_render',
			'unitelocale_formations',
			'crf_formation_add_date_section',
			array('label_for' => 'detail')
		);
	
	}
	
	// -------------------------------------------------------------------------------------
	// WRITER
	// -------------------------------------------------------------------------------------
	
	static function write_data_type($id)
	{
		$data = array(
			'nom_long' => $_POST['nom_long'],
			'description' => $_POST['description'],
			'grand_public' => $_POST['grand_public'] ? '1' : '0',
			'obsolete' => $_POST['obsolete'] ? '1' : '0'
		);
		global $wpdb;
		if ($_POST['action']=='ajout_type_formation')
		{
			$data['nom'] = $_POST['nom'];
			if (empty($data['nom']))
			{
				throw new Exception('Le nom de la formation ne peut pas être vide !');
			}
			$wpdb->insert(
				$wpdb->base_prefix.'crf_formations_type',
				$data,
				array('%s', '%s', '%d', '%d', '%s')
			);
		}
		elseif($_POST['id'])
		{
			$wpdb->update(
				$wpdb->base_prefix.'crf_formations_type',
				$data,
				array(
					'id' => $_POST['id']
				),
				array('%s', '%s', '%d', '%d'),
				array('%d')
			);
		}
	}

	static function write_data_date($id)
	{
		$data = array(
			'type' => $_POST['type_formation'],
			'date_debut' => $_POST['date_debut']['date'].' '.$_POST['date_debut']['time'],
			'date_fin' => $_POST['date_fin']['date'].' '.$_POST['date_fin']['time'],
			'detail' => $_POST['detail']
		);
		global $wpdb;
		if ($_POST['action']=='ajout_date_formation')
		{
			if (empty($data['type']))
			{
				throw new Exception('Le type de la formation ne peut pas être vide !');
			}
			$wpdb->insert(
				$wpdb->base_prefix.'crf_formations',
				$data,
				array('%d', '%s', '%s', '%s')
			);
		}
		elseif($_POST['id'])
		{
			$wpdb->update(
				$wpdb->base_prefix.'crf_formations',
				$data,
				array(
					'id' => $_POST['id']
				),
				array('%d', '%s', '%s', '%s'),
				array('%d')
			);
		}
	}
	
	// -------------------------------------------------------------------------------------
	// RENDERS
	// -------------------------------------------------------------------------------------
	

	/**
	 * Render de la page de la page
	 */
	static function options_page()
	{
		$action = isset($_GET['action']) ? strtolower($_GET['action']) : 'view';
		if (!empty($_POST) && isset($_GET['write']) && $_GET['write'] == 1 && isset($_GET['action']))
		{
			$url = remove_query_arg(array('write', 'id', 'action'));
			if ($action == 'edit_date' || $action=='ajout_date_formation')
			{
				self::write_data_date(isset($_GET['id']) ? $_GET['id'] : 0);
				$url = add_query_arg('tab', 'date', $url);
			}
			elseif ($action == 'edit_type' || $action=='ajout_type_formation')
			{
				self::write_data_type(isset($_GET['id']) ? $_GET['id'] : 0);
				$url = add_query_arg('tab', 'type', $url);
			}
			?>
			<script type="text/javascript">
				window.location="<?php echo $url ?>";
			</script>
			<?php
			return;
		}
		?>
		<h2>Formations</h2>
		<div id="icon-themes" class="icon32" id="icon-options-general"></div>
		<?php
		switch ($action)
		{
			case 'edit_date':
				self::$_item = $GLOBALS['wpdb']->get_row('select * from '.$GLOBALS['wpdb']->base_prefix.'crf_formations where id='.$_GET['id'], 0);
			case 'ajout_date_formation':
				self::form_date_render($action=='ajout_date_formation');
				break;
				
			case 'edit_type' :
				self::$_item = $GLOBALS['wpdb']->get_row('select * from '.$GLOBALS['wpdb']->base_prefix.'crf_formations_type where id='.$_GET['id'], 0);
			case 'ajout_type_formation':
				self::form_type_render($action=='ajout_type_formation');
				break;
			
			default:
				self::viewer_render($action);
				break;
		}
	}

	// -------------------------------------------------------------------------------------
	// RENDERS DE SECTION
	// -------------------------------------------------------------------------------------
	
	/**
	 * Render des vues
	 */
	static function viewer_render()
	{
		$tab    = isset($_GET['tab']) ? strtolower($_GET['tab']) : 'date';
		?>
		<h3 class="nav-tab-wrapper">
			<a href="<?php menu_page_url('unitelocale_formations')?>&tab=date" class="nav-tab<?php echo $tab=='date' ? ' nav-tab-active' : '' ?>">Les dates</a>
			<a href="<?php menu_page_url('unitelocale_formations')?>&tab=type" class="nav-tab<?php echo $tab=='type' ? ' nav-tab-active' : '' ?>">Les formations</a>
		</h3>
		<div class="wrap">
		<?php	
	    switch ($tab)
	    {	    	
	    	default:
	        case 'date':
	        	self::tab_formations_date_render();
				break;
				
	        case 'type':
	        	self::tab_formations_types_render();
	            break;
	    }
	    ?>
		</div>
		<?php
	}

	/**
	 * Render du formulaire d'une date de formation
	 * @param boolean $adding true si formulaire d'ajout. false si formulaire d'édition
	 */
	static function form_date_render($adding)
	{
		?>
		<h3><?php echo $adding ? 'Ajouter' : 'Modifier'?> une session de formation</h3>
		<form action='<?php echo add_query_arg('write', '1') ?>' method='post'>
			<input type="hidden" name="action" value="<?php echo isset($_GET['action']) ? $_GET['action'] : '' ?>" />
			<input type="hidden" name="id" value="<?php echo isset(self::$_item) ? self::$_item->id : '' ?>" />
			<table class="form-table">
			<?php
				do_settings_fields('unitelocale_formations', 'crf_formation_add_date_section');
			?>
			</table>
			<?php submit_button(); ?>
		</form>
		<?php
	}
	
	/**
	 * Render du formulaire d'un type de formation
	 * @param boolean $adding true si formulaire d'ajout. false si formulaire d'édition
	 */
	static function form_type_render($adding)
	{
		?>
		<h3><?php echo $adding ? 'Ajouter' : 'Modifier'?> un type de formation</h3>
		<form action='<?php echo add_query_arg('write', '1') ?>' method='post'>
			<input type="hidden" name="action" value="<?php echo isset($_GET['action']) ? $_GET['action'] : '' ?>" />
			<input type="hidden" name="id" value="<?php echo isset(self::$_item) ? self::$_item->id : '' ?>" />
			<table class="form-table">
				<?php do_settings_fields('unitelocale_formations', 'crf_formation_add_type_section'); ?>
			</table>
			<?php submit_button(); ?>
		</form>
		<?php
	}
	
	// -------------------------------------------------------------------------------------
	// FORMULAIRE TYPE DE FORMATION
	// -------------------------------------------------------------------------------------
	
	static function nom_render($arg)
	{
		?>
		<input type="text" name="<?php echo $arg['label_for'] ?>" id="<?php echo $arg['label_for'] ?>" value="<?php echo self::$_item ? self::$_item->nom : ''?>" <?php echo self::$_item ? 'readonly="readonly" disabled="disabled"' : '' ?> placeholder="PSC1, PSE2, etc." required="required" />
		<?php
	}

	static function nom_long_render($arg)
	{
		?>
		<input type="text" name="<?php echo $arg['label_for'] ?>" id="<?php echo $arg['label_for'] ?>" value="<?php echo self::$_item ? self::$_item->nom_long : ''?>" placeholder="Nom complet de la formation. Par ex.: Prévention &amp; secours civiques de niveau 1"  required="required" />
		<?php
	}

	static function description_render($arg)
	{
		?>
		<textarea name="<?php echo $arg['label_for'] ?>" id="<?php echo $arg['label_for'] ?>" placeholder="Description détaillée de la formation : sa durée générale, son tarif, etc. Par ex.: &quot;Apprendre à sauver des vies en quelques heures ! Formation en 10 à 12h sur une ou deux journée pour 60 EUR / pers.&quot;"><?php echo self::$_item ? self::$_item->description : ''?></textarea>
		<?php
	}

	static function grand_public_render($arg)
	{
		?>
		<input type="checkbox" name="<?php echo $arg['label_for'] ?>" id="<?php echo $arg['label_for'] ?>" <?php echo self::$_item && self::$_item->grand_public ? 'checked="checked" ' : ''?>/>
		<?php
	}

	
	static function obsolete_render($arg)
	{
		?>
		<input type="checkbox" name="<?php echo $arg['label_for'] ?>" id="<?php echo $arg['label_for'] ?>" <?php echo self::$_item && self::$_item->obsolete ? 'checked="checked" ' : ''?>/>
		<?php
	}
		
	// -------------------------------------------------------------------------------------
	// FORMULAIRE DATE DE FORMATION
	// -------------------------------------------------------------------------------------
		
	static function type_formation_render($arg)
	{
		global $wpdb;
		$type_formations_list = $wpdb->get_results('select id, nom, grand_public from '.$wpdb->base_prefix.'crf_formations_type where obsolete = 0 order by grand_public asc');
		$grand_public_list = array();
		$interne_list = array();
		foreach($type_formations_list as $type_formation)
		{
			if ($type_formation->grand_public)
			{
				$grand_public_list[$type_formation->id] = $type_formation->nom;
			}
			else
			{
				$interne_list[$type_formation->id] = $type_formation->nom;
			}
		}
		?>
		<select name="<?php echo $arg['label_for']?>" id="<?php echo $arg['label_for']?>">
			<optgroup label="Grand public">
			<?php foreach($grand_public_list as $id => $nom): ?>
				<option value="<?php echo $id?>" data-nom="<?php echo $nom ?>" <?php echo (!empty(self::$_item) && self::$_item->type == $id) ? 'selected="selected"' : ''?>><?php echo $nom ?></option>
			<?php endforeach; ?>	
			</optgroup>
			<optgroup label="Interne">
			<?php foreach($interne_list as $id => $nom): ?>
				<option value="<?php echo $id?>" data-nom="<?php echo $nom ?>"><?php echo $nom ?></option>
			<?php endforeach; ?>	
			</optgroup>
		</select>
		<?php
	}

	static function date_debut_render($arg)
	{
		?>
		<input type="date" id="<?php echo $arg['label_for']?>" name="<?php echo $arg['label_for']?>[date]" value="<?php echo !empty(self::$_item) && self::$_item->date_debut ? date('Y-m-d', strtotime(self::$_item->date_debut)) : '' ?>" /><input type="time" name="<?php echo $arg['label_for']?>[time]" value="<?php echo !empty(self::$_item) && self::$_item->date_debut ? date('H:m:s', strtotime(self::$_item->date_debut)) : '' ?>"/>
		<?
	}
	
	static function date_fin_render($arg)
	{
		?>
		<input type="date" id="<?php echo $arg['label_for']?>" name="<?php echo $arg['label_for']?>[date]" value="<?php echo !empty(self::$_item) && self::$_item->date_fin ? date('Y-m-d', strtotime(self::$_item->date_fin)) : '' ?>" /><input type="time" name="<?php echo $arg['label_for']?>[time]" value="<?php echo !empty(self::$_item) && self::$_item->date_fin ? date('H:m:s', strtotime(self::$_item->date_fin)) : '' ?>"/>
		<?
	}
	
	static function detail_render($arg)
	{
		?>
		<textarea id="<?php echo $arg['label_for']?>" name="<?php echo $arg['label_for']?>" placeholder="Détail de cette session de formation : déroulement, horaires des sessions, matériel, pré-requis, etc."><?php echo !empty(self::$_item) && self::$_item->detail ? self::$_item->detail : '' ?></textarea>
		<?
	}
	
	// -------------------------------------------------------------------------------------
	// ONGLETS
	// -------------------------------------------------------------------------------------
		
	/**
	 * Render de la liste des dates de formations
	 */
	static function tab_formations_date_render()
	{		
		if (! class_exists('CRF_List_DateFormation'))
		{
			require_once  plugin_dir_path( __FILE__ ).'../includes/class-crf-list-date-formation.php';
		}
		$table = new CRF_List_DateFormation();
		?>
		<p>Indiquez vos dates de formation à venir</p>
		<p>
			<a href="<?php menu_page_url('unitelocale_formations')?>&action=ajout_date_formation" class="page-title-action">Ajouter une date</a>
		</p>
		<?php $table->prepare_items(); ?>
		<?php $table->views(); ?>
		<?php $table->display(); ?>
		<!-- <button type="button" id="crf_formation_ajouter">Ajouter une date de formation</button> -->
		<?php 
	}

	/**
	 * Render de la liste des types de formations
	 */
	static function tab_formations_types_render()
	{
		if (! class_exists('CRF_List_TypeFormation'))
		{
			require_once  plugin_dir_path( __FILE__ ).'../includes/class-crf-list-type-formation.php';
		}
		$table = new CRF_List_TypeFormation();
		?>
		<p>Liste de formations dispensées</p>
		<p>
			<a href="<?php menu_page_url('unitelocale_formations')?>&action=ajout_type_formation" class="page-title-action">Ajouter une formation</a>
		</p>
		<?php $table->prepare_items(); ?>
		<?php $table->views(); ?>
		<?php $table->display(); ?>
		<!-- <button type="button" id="crf_formation_type_ajouter">Ajouter un type de formation</button> -->
		<?php 
	}
	
	// -------------------------------------------------------------------------------------
	// STYLE
	// -------------------------------------------------------------------------------------
		
	/**
	 * Ajout CSS
	 */
	public static function add_style()
	{
		wp_register_style('css-plugin-crf-formation', plugin_dir_url(dirname(__FILE__)).'css/admin.css');
		wp_enqueue_style( 'css-plugin-crf-formation' );
	}
	
} // END CLASS


add_action( 'admin_menu', 'CRF_Formations_Plugin_Admin::add_admin_menu', 11);
add_action( 'admin_init', 'CRF_Formations_Plugin_Admin::init', 11);
add_action( 'admin_print_styles', 'CRF_Formations_Plugin_Admin::add_style');
