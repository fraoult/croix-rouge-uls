<?php
if (! class_exists('WP_List_Table'))
{
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class CRF_List_Table extends WP_List_Table
{
	const START_UNORDERED = false;
	const START_ASC = true;
	
	private $_columns = array();
	private $_hidden_columns = array();
	private $_sortable_columns = array();
	private $_bulk_actions = array();
	private $_data = array();
	private $_actions = array();
	private $_id_column = 'id';
	private $_itemPerPage = NULL;
	private $_primary_column = NULL;
	
	// -------------------------------------
	// SETTERS
	// -------------------------------------
	
	/**
	 * @param array $columns
	 * @return CRF_List_Table
	 */
	public function set_column(array $columns)
	{
		$this->_columns = is_array($columns) ? $columns : array();
		return $this;
	}

	/**
	 * @param array $hidden_columns
	 * @return CRF_List_Table
	 */
	public function set_hidden_column(array $hidden_columns)
	{
		$this->_hidden_columns = is_array($hidden_columns) ? $hidden_columns : array();
		return $this;
	}
	
	/**
	 * @param array $sortable_columns
	 * @return CRF_List_Table
	 */	
	public function set_sortable_columns(array $sortable_columns)
	{
		$this->_sortable_columns = is_array($sortable_columns) ? $sortable_columns : array();
		return $this;
	}

	/**
	 * @param array $bulk_actions
	 * @return CRF_List_Table
	 */
	public function set_bulk_actions(array $bulk_actions)
	{
		$this->_bulk_actions = is_array($bulk_actions) ? $bulk_actions : array();
		return $this;
	}
	
	/**
	 * @param array $data
	 * @return CRF_List_Table
	 */
	public function set_data(array $data)
	{
		$this->_data = is_array($data) ? $data : array();
		return $this;
	}
	
	/**
	 * @param string $id_column
	 * @return CRF_List_Table
	 */
	public function set_id_column($id_column='id')
	{
		$this->_id_column = $id_column;
		return $this;
	}
	
	/**
	 * @param string $primary_column
	 */
	public function set_primary_column($primary_column = NULL)
	{
		$this->_primary_column = $primary_column;
	}

	/**
	 * @param string $column_name
	 * @param callback $callback
	 * @return CRF_List_Table
	 */
	public function set_render_column($column_name, $callback)
	{
		$this->{'column_'.$column_name} = $callback;
		return $this;
	}

	/**
	 * @param array $actions
	 * @return CRF_List_Table
	 */
	public function set_row_actions(array $actions)
	{
		$this->_actions = $actions;
		return $this;
	}
	
	// -------------------------------------
	// GETTERS
	// -------------------------------------
	
	/**
	 * @see WP_List_Table::get_columns()
	 */
	public function get_columns()
	{
		return $this->_columns;
	}

	/**
	 * @return array
	 */
	public function get_hidden_columns()
	{
		return $this->_hidden_columns;
	}
	
	/**
	 * @see WP_List_Table::get_sortable_columns()
	 */
	public function get_sortable_columns()
	{
		return $this->_sortable_columns;
	}
	
	/**
	 * @see WP_List_Table::get_bulk_actions()
	 */
	public function get_bulk_actions()
	{
		return $this->_bulk_actions;
	}
	
	/**
	 * @see WP_List_Table::get_primary_column_name()
	 */
	protected function get_primary_column_name()
	{
		if ($this->_primary_column === NULL)
		{
			return parent::get_primary_column_name();
		}
		return $this->_primary_column;
	}
	
	// -------------------------------------
	// OVERRIDE
	// -------------------------------------
	
	/**
	 * @see WP_List_Table::column_default()
	 */
	public function column_default($item, $column_name)
	{
		if (isset($item[$column_name]))
		{
			return $item[$column_name];
		}
		return print_r($item, true);
	}
	
	/**
	 * @see WP_List_Table::column_cb()
	 */
	public function column_cb($item)
	{
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item[$this->_id_column]
		);
	}
	
	/**
	 * @see WP_List_Table::prepare_items()
	 */
	public function prepare_items()
	{
		$columns = $this->get_columns();
		$hidden = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$primary = $this->get_primary_column_name();
        $this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $this->_data;
		if ($_itemPerPage > 0 && count($this->items) > 0)
		{
			$this->set_pagination_args(array(
				'total_items' => count($this->items),
				'per_page'    => $this->_itemPerPage
			));
			$current_page = $this->get_pagenum();
			$this->items = array_slice($this->items, (($current_page-1)*$this->_itemPerPage), $this->_itemPerPage);
		}
		if (!empty($this->_actions) && !empty($primary))
		{
			$this->{'column_'.$primary} = function($item)
			{
				return '<strong>'.$item[$primary].'</strong>'.$this->row_actions($this->_actions);
			};
		}
	}
	

	// -------------------------------------
	// MISC.
	// -------------------------------------
	
	/**
	 * @param array $item
	 * @return string
	 */
	public function column_id($item)
	{
		return $item[$this->_id_column];
	}
	
	/**
	 * @see WP_List_Table::__call()
	 */
	public function __call($name, $arguments)
	{
		if (substr($name,0,7)==='column_' && isset($this->$name))
		{
			$method = $this->name;
			return call_user_func_array($method, $arguments);
		}
		return parent::__call($name, $arguments);
	}
	
	/**
	 * @see WP_List_Table::__set()
	 */
	public function __set($name, $value)
	{
		if (substr($name,0,7)==='column_')
		{
			$this->$name = $value;
			return;
		}
		parent::__set($name, $value);
	}

}