<?php
if (! class_exists('WP_List_Table'))
{
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class CRF_List_DateFormation extends WP_List_Table
{

	function __construct($args=array())
	{
		parent::__construct(array(
			'plural' => __('Dates de formation', 'crf'),
			'singular' => __('Date de formation', 'crf'),
			'ajax' => false,
			'screen' => null
		));
	}
	
	// -------------------------------------
	// GETTERS
	// -------------------------------------
	
	/**
	 * @see WP_List_Table::get_columns()
	 */
	public function get_columns()
	{
		return array(
			'cb' => '<input type="checkbox" />',
			'nom' => __('Formation', 'crf'),
			'date_debut' => __('Début', 'crf'),
			'date_fin' => __('Fin', 'crf'),
			'detail' => __('Détail', 'crf'),
		);
	}

	/**
	 * @return array
	 */
	public function get_hidden_columns()
	{
		return array();
	}
	
	/**
	 * @see WP_List_Table::get_views()
	 */
	protected function get_views()
	{

		$current = $this->_get_current_view();
		
		$url_all  = add_query_arg('view', 'all');
		$url_next = add_query_arg('view', 'next');
		
		global $wpdb;
		$result = $wpdb->get_results('select if(date_debut >= NOW(), 1, 0) as next, count(id) as nb from '.$wpdb->prefix.'crf_formations group by next');
		$count = array('next' => 0, 'all' => 0);
		foreach($result as $item)
		{
			$count[$item->next ? 'next' : 'prev'] = (int)$item->nb;
		}
		$count['all'] = array_sum($count);
		
		return array(
			'all'  => '<a href="'.$url_all.'"'.($current=='all' ? 'class="current"' : '').'>Toutes</a> <span class="count">('.$count['all'].')</span>',
			'next' => '<a href="'.$url_next.'"'.($current=='next' ? 'class="current"' : '').'>A venir</a> <span class="count">('.$count['next'].')</span>',
		);
	}

	
	/**
	 * @see WP_List_Table::get_sortable_columns()
	 */
	protected function get_sortable_columns()
	{
		return array(
			'nom' => array('nom', true),
			'date_debut' => array('date_debut', false),
			'date_fin' => array('date_fin', false)
		);
	}
	
	/**
	 * @see WP_List_Table::get_bulk_actions()
	 */
	protected function get_bulk_actions()
	{
		return array(
			'delete' => 'Supprimer'
		);
	}

	/**
	 * @return string
	 */
	protected function _get_current_view()
	{
		return isset($_GET['view']) ? strtolower($_GET['view']) : 'next';
	}
	
	// -------------------------------------
	// OVERRIDE
	// -------------------------------------
	
	/**
	 * @see WP_List_Table::no_items()
	 */
	public function no_items()
	{
		if ($this->_get_current_view() == 'next')
		{
			echo '- Aucune formation à venir -';
		}
		else
		{
			echo '- Aucune formation existante -';
		}
	}
	
	/**
	 * @see WP_List_Table::column_default()
	 */
	public function column_default($item, $column_name)
	{
		if (array_key_exists($column_name, $item))
		{
			return $item[$column_name];
		}
		return print_r($item, true);
	}
	
	
	/**
	 * @see WP_List_Table::prepare_items()
	 */
	public function prepare_items()
	{
		$columns = $this->get_columns();
		$hidden = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$primary = $this->get_primary_column_name();
        $this->_column_headers = array($columns, $hidden, $sortable, $primary);
        $this->set_items();
	}

	/**
	 * 
	 */
	protected function set_items()
	{
		$order = '';
		if (isset($_GET['orderby']))
		{
			$order = ' order by `'.$_GET['orderby'].'`';
			if (isset($_GET['order']))
			{
				$order .= ' '.$_GET['order'];
			}
		}
		
		$where = $this->_get_where();
		
		global $wpdb;
		$this->items = $wpdb->get_results('select t.nom, f.id as id, unix_timestamp(f.date_debut) as date_debut, unix_timestamp(f.date_fin) as date_fin, f.detail from '.$wpdb->prefix.'crf_formations f join '.$wpdb->prefix.'crf_formations_type t on f.type = t.id '.$where.$order, ARRAY_A);
	}
	
	/**
	 * 
	 * @param string $view
	 * @return string
	 */
	protected function _get_where($view = NULL)
	{
		if (empty($view))
		{
			$view = $this->_get_current_view();
		}
		
		switch ($view)
		{
			case 'all':
				return '';
			
			case 'next':
			default:
				return ' where date_debut >= NOW()'; // - INTERVAL 1 MONTH';
		}
	}
	
	/**
	 * 
	 * @param string $view
	 * @return number
	 */
	protected function _count_items_by_view($view)
	{
		$where = $this->_get_where($view);
		
		global $wpdb;
		return (int)$wpdb->get_var('select count(id) from '.$wpdb->prefix.'crf_formations'.$where, 0, 0);
	}

	protected function column_cb($item) {
		return '<input type="checkbox" name="formations[]" value="'.$item['id'].'" />';
	}
	
	protected function column_nom($item)
	{
		$url = add_query_arg(array(
			'page'   => $_REQUEST['page'],
			'action' => 'edit_date',
			'id' => $item['id']
		), '');
		
		$actions = array(
			'edit' => '<a href="'.$url.'">'.__('Modifier', 'crf').'</a>'
		);
		
		return '<strong><a href="'.$url.'">'.$item['nom'].'</a></strong>'.$this->row_actions($actions);
		
	}

	protected function column_date_debut($item)
	{
		echo ucfirst(strftime('%a %d %b %Y, %H:%M', $item['date_debut']));
	}

	protected function column_date_fin($item)
	{
		echo ucfirst(strftime('%a %d %b %Y, %H:%M', $item['date_fin']));
	}
	
	/*
	protected function _column_obsolete($item, $classes, $data, $primary)
	{
		$attributes = 'class="'.$classes.'" '.$data.' data-type="boolean" data-value="'.$item['obsolete'].'"';
	
		echo "<td $attributes>";
		echo $this->column_obsolete($item);
		echo $this->handle_row_actions( $item, $column_name, $primary );
		echo "</td>";
	
	}
	
	protected function column_grand_public($item)
	{
		return $item['grand_public'] ? 'OUI' : 'NON';
	}
	
	protected function column_obsolete($item)
	{
		return $item['obsolete'] ? 'OUI' : 'NON';
	}
	*/
	

}