<?php
if (! class_exists('WP_List_Table'))
{
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class CRF_List_TypeFormation extends WP_List_Table
{

	function __construct($args=array())
	{
		parent::__construct(array(
			'plural' => __('Types de formation', 'crf'),
			'singular' => __('Type de formation', 'crf'),
			'ajax' => false,
			'screen' => null
		));
	}
	
	// -------------------------------------
	// GETTERS
	// -------------------------------------
	
	/**
	 * @see WP_List_Table::get_columns()
	 */
	public function get_columns()
	{
		return array(
			'cb' => '<input type="checkbox" />',
			'nom' => __('Nom court / Acronyme', 'crf'),
			'nom_long' => __('Nom long', 'crf'),
			'description' => __('Description affichée', 'crf'),
			'grand_public' => __('Grand public ?', 'crf'),
			'obsolete' => __('Obsolète ?', 'crf')
		);
	}

	/**
	 * @return array
	 */
	public function get_hidden_columns()
	{
		return array();
	}
	
	/**
	 * @see WP_List_Table::get_views()
	 */
	protected function get_views()
	{
		
		$current = $this->_get_current_view();
		
		$url_all = add_query_arg('view', 'all');
		$url_gp  = add_query_arg('view', 'gp');
		$url_crf = add_query_arg('view', 'crf');
		
		global $wpdb;
		$result = $wpdb->get_results('select grand_public, count(id) as nb from '.$wpdb->prefix.'crf_formations_type group by grand_public');
		
		$count = array('all' => 0, 'gp' => 0, 'crf' => 0);
		foreach($result as $item)
		{
			$count[$item->grand_public ? 'gp' : 'crf'] = (int)$item->nb;
		}
		$count['all'] = array_sum($count);
		
		return array(
			'all'  => '<a href="'.$url_all.'"'.($current=='all' ? 'class="current"' : '').'>Toutes</a> <span class="count">('.$count['all'].')</span>',
			'gp'   => '<a href="'.$url_gp.'"'.($current=='gp' ? 'class="current"' : '').'>Grand public</a> <span class="count">('.$count['gp'].')</span>',
			'crf'  => '<a href="'.$url_crf.'"'.($current=='crf' ? 'class="current"' : '').'>Internes</a> <span class="count">('.$count['crf'].')</span>',
		);
		
	}
	
	
	/**
	 * @see WP_List_Table::get_sortable_columns()
	 */
	protected function get_sortable_columns()
	{
		return array(
			'nom' => array('nom', true),
			'grand_public' => array('grand_public', false),
			'obsolete' => array('obsolete', false)
		);
	}
	
	/**
	 * @see WP_List_Table::get_bulk_actions()
	 */
	protected function get_bulk_actions()
	{
		return array(
			'set_obsolete' => 'Rendre obsolete',
			'unset_obsolete' => 'Rendre valide',
			'set_gp' => 'Rendre grand public',
			'unset_gp' => 'Restreindre aux bénévole'
		);
	}

	/**
	 * @return string
	 */
	protected function _get_current_view()
	{
		return isset($_GET['view']) ? strtolower($_GET['view']) : 'all';
	}
	
	
	// -------------------------------------
	// OVERRIDE
	// -------------------------------------
	
	/**
	 * @see WP_List_Table::column_default()
	 */
	public function column_default($item, $column_name)
	{
		if (isset($item[$column_name]))
		{
			return $item[$column_name];
		}
		return print_r($item, true);
	}
	
	
	/**
	 * @see WP_List_Table::prepare_items()
	 */
	public function prepare_items()
	{
		$columns = $this->get_columns();
		$hidden = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$primary = $this->get_primary_column_name();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $order = '';
        if (isset($_GET['orderby']))
        {
        	$order = ' order by `'.$_GET['orderby'].'`';
        	if (isset($_GET['order']))
        	{
        		$order .= ' '.$_GET['order'];
        	}
        }
        $this->_set_items();
	}
	
	/**
	 *
	 */
	protected function _set_items()
	{
		$order = '';
	        if (isset($_GET['orderby']))
        {
        	$order = ' order by `'.$_GET['orderby'].'`';
        	if (isset($_GET['order']))
        	{
        		$order .= ' '.$_GET['order'];
        	}
        }
        
        $where = $this->_get_where();
		global $wpdb;
		$this->items = $wpdb->get_results('select * from '.$wpdb->prefix.'crf_formations_type'.$where.$order, ARRAY_A);
	}
	
	/**
	 *
	 * @param string $view
	 * @return string
	 */
	protected function _get_where($view = NULL)
	{
		if (empty($view))
		{
			$view = $this->_get_current_view();
		}
	
		switch ($view)
		{
			case 'crf':
				return ' where grand_public = 0';
				
			case 'gp':
				return ' where grand_public = 1';
				
			case 'all':
			default:
				return '';
		}
	}
	
	/**
	 *
	 * @param string $view
	 * @return number
	 */
	protected function _count_items_by_view($view)
	{
		$where = $this->_get_where($view);
		global $wpdb;
		return (int)$wpdb->get_var('select count(id) from '.$wpdb->prefix.'crf_formations_type'.$where, 0, 0);
	}
	

	protected function column_cb($item) {
		return '<input type="checkbox" name="type_formations[]" value="'.$item['id'].'" />';
	}
	
	protected function column_nom($item)
	{
		$url = add_query_arg(array(
			'page'   => $_REQUEST['page'],
			'action' => 'edit_type',
			'id' => $item['id']
		), '');
		
		$actions = array(
			'edit' => '<a href="'.$url.'">'.__('Modifier', 'crf').'</a>'
		);
		
		return '<strong><a href="'.$url.'">'.$item['nom'].'</a></strong>'.$this->row_actions($actions);
		
	}
	
	protected function _column_grand_public($item, $classes, $data, $primary)
	{
		$attributes = 'class="'.$classes.'" '.$data.' data-type="boolean" data-value="'.$item['grand_public'].'"';
		
		echo "<td $attributes>";
		echo $this->column_grand_public($item);
		echo $this->handle_row_actions( $item, $column_name, $primary );
		echo "</td>";
		
	}

	protected function _column_obsolete($item, $classes, $data, $primary)
	{
		$attributes = 'class="'.$classes.'" '.$data.' data-type="boolean" data-value="'.$item['obsolete'].'"';
	
		echo "<td $attributes>";
		echo $this->column_obsolete($item);
		echo $this->handle_row_actions( $item, $column_name, $primary );
		echo "</td>";
	
	}
	
	protected function column_grand_public($item)
	{
		return $item['grand_public'] ? 'OUI' : 'NON';
	}
	
	protected function column_obsolete($item)
	{
		return $item['obsolete'] ? 'OUI' : 'NON';
	}
	

}