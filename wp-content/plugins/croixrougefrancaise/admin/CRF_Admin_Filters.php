<?php

/**
 * Les filtres back-office
 * @author fraoult
 * 
 */
class CRF_Admin_Filters
{
	
	/**
	 * Nom du site
	 * @param  string $default_val
	 * @return string
	 */
	static function option_blogname($default_val)
	{
		if (defined('OPTION_BLOGNAME'))
		{
			return OPTION_BLOGNAME;
		}
		else
		{
			return $default_val;
		}
	}
	
	/**
	 * Slogan
	 * @param  string $default_val
	 * @return string
	 */
	static function option_blogdescription($default_val)
	{
		if (defined('OPTION_BLOGDESCRIPTION'))
		{
			return OPTION_BLOGDESCRIPTION;
		}
		else
		{
			return $default_val;
		}
	}
	
	/**
	 * Format de date
	 * @param  string $default_val
	 * @return string
	 */
	static function option_dateformat($default_val)
	{
		if (defined('OPTION_DATEFORMAT'))
		{
			return OPTION_DATEFORMAT;
		}
		else
		{
			return $default_val;
		}
	}
	
	/**
	 * Format horaire
	 * @param  string $default_val
	 * @return string
	 */
	static function option_timeformat($default_val)
	{
		if (defined('OPTION_TIMEFORMAT'))
		{
			return OPTION_TIMEFORMAT;
		}
		else
		{
			return $default_val;
		}
	}
	
	/**
	 * Fuseau horaire
	 * @param  string $default_val
	 * @return string
	 */
	static function option_timezone($default_val)
	{
		if (defined('OPTION_TIMEZONE'))
		{
			return OPTION_TIMEZONE;
		}
		else
		{
			return $default_val;
		}
	}
	
} // END CLASS

add_filter('option_blogname'           , 'CRF_Admin_Filters::option_blogname');
add_filter('option_blogdescription'    , 'CRF_Admin_Filters::option_blogdescription');
add_filter('option_date_format'        , 'CRF_Admin_Filters::option_dateformat');
add_filter('option_time_format'        , 'CRF_Admin_Filters::option_timeformat');
add_filter('option_timezone_string'    , 'CRF_Admin_Filters::option_timezone');
