<?php

/**
 * Placement des établissements sur la carte
 * @author fraoult
 * 
 */
class CRF_Settings_Map_Etablissement
{
	
	/**
	 * Ajouter les infos google
	 * @param  array $value
	 * @param  array $old_value
	 * @return array
	 */
	static function add_place_id($value, $old_value)
	{
		if (isset($value['etablissements']) && is_array($value['etablissements']))
		{
			foreach($value['etablissements'] as $idx => $etablissement)
			{
				$value['etablissements'][$idx]['place_id'] = '';
				$result = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($etablissement['adresse']));
				if (empty($result))
				{
					continue;
				}
				$result = json_decode($result, TRUE);
				if (! $result || ! is_array($result))
				{
					continue;
				}
				if (! isset($result['results']) || count($result['results']) == 0)
				{
					continue;
				}
				$addr = reset($result['results']);
				$value['etablissements'][$idx]['lat'] = round($addr['geometry']['location']['lat'],6);
				$value['etablissements'][$idx]['lon'] = round($addr['geometry']['location']['lng'],6);
				$value['etablissements'][$idx]['google_formatted_address'] = $addr['formatted_address'];
			}
		}
		return $value;
	}
	
	/**
	 * Mise à jour des marqueurs dans la map
	 * @param  array $old_value
	 * @param  array $value
	 * @return array
	 */
	static function update_markers($old_value, $value)
	{
		global $wpdb;
	
		$id_list  = array();
		$row_list = $wpdb->get_results('SELECT `id` FROM '.$wpdb->prefix.'leafletmapsmarker_markers WHERE `updatedby` LIKE "__CRF_THEME_SYSTEM__"', ARRAY_A);
		foreach($row_list as $row)
		{
			$id_list[] = $row['id'];
		}
	
		// Supprimer les anciens marqueurs placés automatiquement
	
		$wpdb->delete($wpdb->prefix.'leafletmapsmarker_markers', array('updatedby' => '__CRF_THEME_SYSTEM__'));
	
		// Dernier ID
	
		$max_id = $wpdb->get_var('SELECT MAX(`id`) as max_id FROM '.$wpdb->prefix.'leafletmapsmarker_markers', 0, 0);
	
		if (empty($id_list) || $max_id < max($id_list))
		{
			$id_list = array();
		}
	
		// Ajouter les adresses à la base
	
		if (isset($value['etablissements']) && is_array($value['etablissements']))
		{
			$id_idx = 0;
			foreach($value['etablissements'] as $idx => $etablissement)
			{
				$markername = CRF_Settings_UL_Etablissements::$etablissements_list[$etablissement['type']]['title'];
				$markericon = CRF_Settings_UL_Etablissements::$etablissements_list[$etablissement['type']]['icon'];
				$upload_dir = wp_upload_dir();
				$dir = $upload_dir['basedir'].'/leaflet-maps-marker-icons';
				$id = $id_idx<count($id_list) ? $id_list[$id_idx++] : (++$max_id);
				if (empty($markericon) || ! file_exists($dir.'/'.$markericon))
				{
					$markericon = 'firstaid.png';
				}
				if (isset($etablissement['tel']) && trim($etablissement['tel'])!='' )
				{
					$etablissement['tel'] = 'Téléphone: <a href="tel:'.$etablissement['tel'].'">'.$etablissement['tel'].'</a>';
				}
				if (isset($etablissement['mail']) && trim($etablissement['mail'])!='')
				{
					$etablissement['mail'] = 'Courriel: <a href="mailto:'.$etablissement['mail'].'">'.$etablissement['mail'].'</a>';
				}
				$wpdb->insert(
						$wpdb->prefix.'leafletmapsmarker_markers',
						array(
								'id'         => $id,
								'markername' => CRF_Settings_UL_Etablissements::$etablissements_list[$etablissement['type']]['title'],
								'basemap'    => 'googleLayer_roadmap',
								'layer'      => '1',
								'lat'        => $etablissement['lat'],
								'lon'        => $etablissement['lon'],
								'icon'       => $markericon,
								'zoom'       => '12',
								//'popuptext'  => trim(join("\n\n", array('<strong>'.CrfUlSettings::$etablissements_list[$etablissement['type']]['title'].'</strong>', $etablissement['extra'], join("\n", array($etablissement['tel'], $etablissement['mail']))))),
								'popuptext'  => trim(join("\n\n", array($etablissement['extra'], join("\n", array($etablissement['tel'], $etablissement['mail']))))),
								'openpopup'  => '0',
								'mapwidth'   => '640',
								'mapwidthunit' => 'px',
								'mapheight'  => '480',
								'panel'      => '1',
								'createdby'  => wp_get_current_user()->user_login,
								'createdon'  => date('Y-m-d H:i:s'),
								'updatedby'  => '__CRF_THEME_SYSTEM__', //wp_get_current_user()->user_login,
								'updatedon'  => date('Y-m-d H:i:s'),
								'controlbox' => '1',
								'overlays_custom'  => '0',
								'overlays_custom2' => '0',
								'overlays_custom3' => '0',
								'overlays_custom4' => '0',
								'wms'   => '0',
								'wms2'  => '0',
								'wms3'  => '0',
								'wms4'  => '0',
								'wms5'  => '0',
								'wms6'  => '0',
								'wms7'  => '0',
								'wms8'  => '0',
								'wms9'  => '0',
								'wms10' => '0',
								'kml_timestamp' => NULL,
								'address'   => $etablissement['google_formatted_address'],
								'gpx_url'   => '',
								'gpx_panel' => '0'
						)
				); // insert();
				if (!empty($wpdb->last_error))
				{
					wp_die($wpdb->last_error);
				}
			}
		}
	
		return $value;
	}
	
} // END CLASS

add_action('pre_update_option_crf_ul_settings', 'CRF_Settings_Map_Etablissement::add_place_id', 10, 2);
add_action('update_option_crf_ul_settings', 'CRF_Settings_Map_Etablissement::update_markers', 10, 2);
