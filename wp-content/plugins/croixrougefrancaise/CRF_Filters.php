<?php

/**
 * Les filtres
 * @author fraoult
 * 
 */
class CRF_Filters
{

		
	/**
	 * Le nom de l'UL devant le titre
	 * @param unknown $value
	 * @return string
	 */
	function wp_title($title)
	{
		return 'Unité locale '.CRF::get_option('nom_ul_avec_preposition').' | '.$title;
	}
	
	
	/**
	 * Variable de configuration dans les textes type {$UL_nom_du_champ_de_config}
	 * @param unknown $content
	 * @return mixed
	*/
	function the_content($content)
	{
		return preg_replace_callback('`{\$CRF_([a-zA-Z0-9_]+)}`', create_function('$matches', 'return crf_get_option($matches[1]);'), $content);
	}
	
	/**
	 * Entourer le contenu d'un article par une section
	 * @param string $content
	 * @return string
	 */
	function wrap_content($content)
	{
		return '<section data-type="main-content">'.PHP_EOL.$content.PHP_EOL.'</section>';
	}
	
	/**
	 * Change le lien vers WP par le lien vers le site web
	 */
	function login_url($url)
	{
		// return bloginfo('url');
		return get_bloginfo('url', 'display');
	}

	/**
	 * Change le titre "Propulsé par WP" par le nom du site web
	 */
	function login_title($title)
	{
		return get_option('blogname');
	}
		
	
} // END CLASS

add_filter('wp_title', 'CRF_Filters::wp_title');
add_filter('the_content', 'CRF_Filters::the_content');
add_filter('the_content', 'CRF_Filters::wrap_content', 1, 1);
add_filter('the_excerpt', 'CRF_Filters::wrap_content', 1, 1);
add_filter('login_headerurl', 'CRF_Filters::login_url');
add_filter('login_headertitle', 'CRF_Filters::login_title');
