<?php

/**
 * Plugin Name: Croix-Rouge Française, Unité Locale
 * Description: Offre une page de configuration spécifique aux unité locale (adresse des différents établissement, localisation sur une carte, etc.).
 * Version: 0.1
 * Author: François RAOULT, Croix-Rouge Française, Délégation départementale des Hauts-de-Seine
 * Author URI: http://92.croix-rouge.fr
 */

// Inclusion des classes

include_once 'CRF.php';
include_once 'CRF_Filters.php';

include_once 'admin/croixrougefrancaise.php';



/**
 * Récupère la valeur d'une option du paramètrage spécifique
 * @param string $setting_name
 * @param string $default_value
 * @return Ambigous <string, string, mixed>
 */

function crf_get_option($setting_name, $default_value = '')
{
	return CRF::get_option($setting_name, $default_value);
}

