<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id="main-core".
 *
 * @package ThinkUpThemes
 */
?>

		</div><!-- #main-core -->
		</div><!-- #main -->
		<?php /* Sidebar */ thinkup_sidebar_html(); ?>
	</div>
	</div><!-- #content -->

	<footer>
		<?php /* Custom Footer Layout */ thinkup_input_footerlayout();
		echo	'<!-- #footer -->';  ?>

 		<div id="footer-contact" class="vcard">
 			<p class="name">Unité locale de <?php echo crf_get_option('nom_ul_avec_preposition') ?></p>
  			<a class="mini-contacts address street-address" href="http://maps.google.fr/?q=<?php echo urlencode(crf_get_option('adresse')) ?>"><?php echo crf_get_option('adresse') ?></a>
  			<a class="mini-contacts phone tel" href="tel:<?php echo str_replace(' ', '', crf_get_option('telephone')) ?>"/><?php echo crf_get_option('telephone'); ?></a>
 			<a class="mini-contacts email" href="<?php echo get_page_link(29);?>">Contactez-nous<?php //echo crf_get_option('mail') ?></a>
		</div>
		<div id="footer-link">
			<p>Site national de la Croix-Rouge française</p>
			<a href="http://www.croix-rouge.fr">http://www.croix-rouge.fr</a>
		</div>

  		<?php /* Social Media Icons */ thinkup_input_socialmedia(); ?>
		
		<div id="sub-footer">
		<div id="sub-footer-core">	

			<?php if ( has_nav_menu( 'sub_footer_menu' ) ) : ?>
			<?php wp_nav_menu( array( 'depth' => 1, 'container_class' => 'sub-footer-links', 'container_id' => 'footer-menu', 'theme_location' => 'sub_footer_menu' ) ); ?>
			<?php endif; ?>
			<!-- #footer-menu -->

			<div class="copyright">
			&copy; Croix-Rouge Française
			</div>
			<!-- .copyright -->

		</div>
		</div>
	</footer><!-- footer -->

</div><!-- #body-core -->

<?php wp_footer(); ?>

</body>
</html>