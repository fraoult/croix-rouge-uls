<?php

class CRF_MenuMetabox
{
	protected static $_type_list = array();
	
	static function add_nav_menu_metabox()
	{
		add_meta_box( 'crf-contact', __( 'Contacts' ), 'CRF_MenuMetabox::nav_menu_metabox', 'nav-menus', 'side', 'default' );
	}

	
	static function init_type_list()
	{
		self::$_type_list = array(
			'#crfAdresse#' => array('title'=>__( 'Adresse' ), 'class' => 'address street-adress'),
			'#crfTelephone#' => array('title'=>__( 'Téléphone' ), 'class' => 'phone tel'),
			'#crfFax#' => array('title'=>__( 'Fax' ), 'class' => 'phone fax'),
			'#crfCourriel#' => array('title'=>__( 'Courriel'), 'class' => 'email'),
		);
	}
	
	static function nav_menu_metabox( $object )
	{
		global $nav_menu_selected_id;

		self::init_type_list();
		
		$items_list = array();
		
		foreach (self::$_type_list as $value => $param)
		{
			$title = $param['title'];
			$items_list[$title] = (object)array(
				'db_id ' => 0,
				'object' => 'custom',
				'object_id' => esc_attr( $value ),
				'menu_item_parent' => 0,
				'type' => 'custom',
				'title' => esc_attr( $title ),
				'url' => esc_attr( $value ),
				'target' => '',
				'attr_title' => '',
				'classes' => explode(' ', $param['class']),
				'xfn' => '',
			);
		}
	
		$walker = new Walker_Nav_Menu_Checklist( array() );
		?>
		<div id="login-links" class="loginlinksdiv">
	
			<div id="tabs-panel-login-links-all" class="tabs-panel tabs-panel-view-all tabs-panel-active">
				<ul id="login-linkschecklist" class="list:login-links categorychecklist form-no-clear">
					<?php echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $items_list ), 0, (object) array( 'walker' => $walker ) ); ?>
				</ul>
			</div>
	
			<p class="button-controls">	
				<span class="add-to-menu">
					<input type="submit"<?php disabled( $nav_menu_selected_id, 0 ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e( 'Add to Menu' ); ?>" name="add-login-links-menu-item" id="submit-login-links" />
					<span class="spinner"></span>
				</span>
			</p>
	
		</div>
		<?php
	}
	

	static function nav_menu_type_label( $menu_item )
	{
		self::init_type_list();
		$type_list = array_keys(self::$_type_list);

		if ( isset($menu_item->object, $menu_item->url) && $menu_item->object == 'custom' && in_array( $menu_item->url, $type_list ) )
		{
			$menu_item->type_label = 'Contact';
		}
	
		return $menu_item;
	}
	
	static function coordonnees( $atts, $item, $args )
	{
		switch($atts['href'])
		{
			case '#crfAdresse#':
				$atts['href'] = 'http://maps.google.fr/?q='.urlencode(crf_get_option('adresse'));
				$item->url = $atts['href'];
				$item->title = crf_get_option('adresse');
				break;
			case '#crfTelephone#':
				$atts['href'] = 'tel:'.str_replace(' ', '', crf_get_option('telephone'));
				$item->url = $atts['href'];
				$item->title = '<span style="white-space: nowrap">'.crf_get_option('telephone').'</span>';
				break;
			case '#crfFax#':
				$atts['href'] = 'tel:'.str_replace(' ', '', crf_get_option('fax'));
				$item->url = $atts['href'];
				$item->title = '<span style="white-space: nowrap">'.crf_get_option('fax').'</span>';
				break;
			case '#crfCourriel#':
				$atts['href'] = 'mailto:'.str_replace(' ', '', crf_get_option('email'));
				$item->url = $atts['href'];
				$item->title = crf_get_option('email');
				break;

		}
		return $atts;
	}
	
	static function coordonnees_class( $classes, $item )
	{
		switch($item->url)
		{
			case '#crfAdresse#':
			case '#crfTelephone#':
			case '#crfFax#':
			case '#crfCourriel#':
				$classes[] = 'mini-contacts';
				break;
		}
		return $classes;
	}
	
}

add_action('admin_head-nav-menus.php', 'CRF_MenuMetabox::add_nav_menu_metabox' );
add_filter('wp_setup_nav_menu_item', 'CRF_MenuMetabox::nav_menu_type_label' );


add_filter( 'nav_menu_link_attributes', 'CRF_MenuMetabox::coordonnees', 10, 3 );
add_filter( 'nav_menu_css_class', 'CRF_MenuMetabox::coordonnees_class', 10, 2 );
