<?php 

include_once 'CRF_MenuMetabox.php';

include_once 'options/00.variables.php';
include_once 'options/02.homepage.php';

function crf_admin_color_palette() {
	wp_admin_css_color(
		'crf-colors',
		__('Croix-Rouge Française'),
		get_stylesheet_directory_uri() . '/css/colors/all/colors.css',
		$GLOBALS['crf_colors'],
		array(
			'current' => $GLOBALS['crf_colors']['red'],
			'focus'   => $GLOBALS['crf_colors']['red'],
			'base'    => $GLOBALS['crf_colors']['black']
		)
	);
	wp_admin_css_color(
		'crf-colors-base',
		__('Croix-Rouge Française (Standard)'),
		get_stylesheet_directory_uri() . '/css/colors/base/colors.css',
		array($GLOBALS['crf_colors']['red'], $GLOBALS['crf_colors']['gray_light'], $GLOBALS['crf_colors']['gray_dark'], $GLOBALS['crf_colors']['black']),
		array('current' => '#6A95A3', 'focus' => '#D10049', 'base' => '#404040')
	);
	wp_admin_css_color(
		'crf-colors-sweet',
		__('Croix-Rouge Française (Secondaire)'),
		get_stylesheet_directory_uri() . '/css/colors/sweet/colors.css',
		array($GLOBALS['crf_colors']['beige_light'], $GLOBALS['crf_colors']['beige_dark'], $GLOBALS['crf_colors']['ocean_light'], $GLOBALS['crf_colors']['ocean_dark']),
		array('current' => '#6A95A3', 'focus' => '#D10049', 'base' => '#404040')
	);
}
add_action('admin_init', 'crf_admin_color_palette');

