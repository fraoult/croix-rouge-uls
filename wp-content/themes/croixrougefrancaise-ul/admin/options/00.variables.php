<?php
/**
 * Theme setup functions.
 *
 * @package ThinkUpThemes
 */


/* ----------------------------------------------------------------------------------
	ADD CUSTOM VARIABLES
---------------------------------------------------------------------------------- */

/* Add global variables used in Redux framework */
function crf_thinkup_reduxvariables() { 

	// Fetch options stored in $data.
	global $redux;

	//  1.1.     General settings.

	//  1.2.     Homepage.
	$GLOBALS['thinkup_homepage_section4_image']             = thinkup_var ( 'thinkup_homepage_section4_image', 'id' );
	$GLOBALS['thinkup_homepage_section4_title']             = thinkup_var ( 'thinkup_homepage_section4_title' );
	$GLOBALS['thinkup_homepage_section4_desc']              = thinkup_var ( 'thinkup_homepage_section4_desc' );
	$GLOBALS['thinkup_homepage_section4_link']              = thinkup_var ( 'thinkup_homepage_section4_link' );

	//  1.3.     Header

	//  1.4.     Footer.

	//  1.5.1.   Blog - Main page.

	//  1.5.2.   Blog - Single post.

	//  1.6.     Portfolio.

	//  1.7.     Contact Page.

	//  1.8.     Special pages.

	//  1.9.     Notification bar.

	//  1.10.     Search engine optimisation.

	//  1.11.     Typography.

	//  1.12.     Custom styling.

	//  1.13.     Support.
}
add_action( 'thinkup_hook_header', 'crf_thinkup_reduxvariables' );

?>