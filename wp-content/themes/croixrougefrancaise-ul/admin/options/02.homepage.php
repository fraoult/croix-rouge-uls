<?php


/**
 * Supprimer des champs de la config. du thème
 */
function crf_remove_theme_fields()
{
	$sections = $GLOBALS['ReduxFramework']->sections;
	foreach($sections as &$section)
	{
		if (! isset($section['fields']) || ! is_array($section['fields']))
		{
			continue;
		}
		foreach($section['fields'] as &$field)
		{
			if ($field['id'] == 'thinkup_general_sitetitle')
			{
				$field = null;
			}
			elseif ($field['id'] == 'thinkup_general_sitedescription')
			{
				$field = null;
			}
			elseif ($field['id'] == 'thinkup_general_logoswitch')
			{
				$field = null;
			}
			elseif ($field['id'] == 'thinkup_general_faviconlink')
			{
				$field = null;
			}
		}
		$section['fields'] = array_filter($section['fields']);
	}
	$GLOBALS['ReduxFramework']->sections = $sections;
}

//add_action('admin_init','crf_remove_theme_fields');


/**
 * Ajouter des champs de la config. du thème
 */
function crf_add_theme_fields()
{
	$sections = $GLOBALS['ReduxFramework']->sections;
	foreach($sections as &$section)
	{
		if (! isset($section['fields']) || ! is_array($section['fields']))
		{
			continue;
		}
		$is_homepage_section = FALSE;
		foreach($section['fields'] as &$field)
		{
			if ($field['id'] == 'thinkup_homepage_sectionswitch')
			{
				$is_homepage_section = TRUE;
				break;
			}
		}
		if ($is_homepage_section)
		{
			$section['fields'][] = array(
				'title'=> __('Content Area 4', 'redux-framework'),
				'desc'=> __('Add an image for the section background.', 'redux-framework'),
				'id'=>'thinkup_homepage_section4_image',
				'type' => 'media',
				'url'=> true,
				'fold' => array('thinkup_homepage_sectionswitch'=>1),
			);
				
			$section['fields'][] = array(
				'id'=>'thinkup_homepage_section4_title',
				'desc' => __('Add a title to the section.', 'redux-framework'),
				'type' => 'text',
				'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
				'fold' => array('thinkup_homepage_sectionswitch'=>1),
			);
				
			$section['fields'][] = array(
				'id'=>'thinkup_homepage_section4_desc',
				'desc' => __('Add some text to featured section 4.', 'redux-framework'),
				'type' => 'textarea',
				'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
				'fold' => array('thinkup_homepage_sectionswitch'=>1),
			);
				
			$section['fields'][] = array(
				'id'=>'thinkup_homepage_section4_link',
				'desc' => __('Link to a page', 'redux-framework'),
				'type' => 'select',
				'data' => 'pages',
				'fold' => array('thinkup_homepage_sectionswitch'=>1),
			);
				
			break;
		}
	}
	$GLOBALS['ReduxFramework']->sections = $sections;
}

//add_action('admin_init','crf_add_theme_fields');


/*
 function crf__add($text)
 {
 echo 'pouet';
 var_dump($text); die;
 return 'truc';
 }
 add_filter( "redux/field/redux/text/render/after", "crf__add", 10, 2); // Adds the local field
 */


//----------------------------------------------------------------------------------
//	ENABLE HOMEPAGE CONTENT
//----------------------------------------------------------------------------------

function crf_extends_thinkup_input_homepagesection()
{
	global $thinkup_homepage_sectionswitch;
	global $thinkup_homepage_section1_image;
	global $thinkup_homepage_section2_image;
	global $thinkup_homepage_section3_image;
	global $thinkup_homepage_section4_image;
	global $thinkup_homepage_section4_title;
	global $thinkup_homepage_section4_desc;
	global $thinkup_homepage_section4_link;

	$thinkup_homepage_section1_image_ = thinkup_var('thinkup_homepage_section1_image', 'id');
	$thinkup_homepage_section2_image_ = thinkup_var('thinkup_homepage_section2_image', 'id');
	$thinkup_homepage_section3_image_ = thinkup_var('thinkup_homepage_section3_image', 'id');
	
	// Call parent function
	ob_start();
	thinkup_input_homepagesection();
	$parent_content = ob_get_clean();

	// Override default values for images
	if ( ! empty( $thinkup_homepage_section1_image_ ) ) {
		$thinkup_homepage_section1_image_ = wp_get_attachment_image_src($thinkup_homepage_section1_image_);
		$parent_content = str_replace('src="'.$thinkup_homepage_section1_image[0].'"', 'src="'.$thinkup_homepage_section1_image_[0].'"', $parent_content);
		$thinkup_homepage_section1_image = $thinkup_homepage_section1_image_;
	}
		
	if ( ! empty( $thinkup_homepage_section2_image_ ) ) {
		$thinkup_homepage_section2_image_ = wp_get_attachment_image_src($thinkup_homepage_section2_image_);
		$parent_content = str_replace('src="'.$thinkup_homepage_section2_image[0].'"', 'src="'.$thinkup_homepage_section2_image_[0].'"', $parent_content);
		$thinkup_homepage_section2_image = $thinkup_homepage_section2_image_;
	}
		
	if ( ! empty( $thinkup_homepage_section3_image_ ) ) {
		$thinkup_homepage_section3_image_ = wp_get_attachment_image_src($thinkup_homepage_section3_image_);
		$parent_content = str_replace('src="'.$thinkup_homepage_section3_image[0].'"', 'src="'.$thinkup_homepage_section3_image_[0].'"', $parent_content);
		$thinkup_homepage_section3_image = $thinkup_homepage_section3_image_;
	}
	
	// Set default values for images
	if ( ! empty( $thinkup_homepage_section4_image ) )
		$thinkup_homepage_section4_image = wp_get_attachment_image_src($thinkup_homepage_section4_image);
	
	// Set default values for titles
	if ( empty( $thinkup_homepage_section4_title ) ) $thinkup_homepage_section4_title = 'Perfect For Red Cross';

	// Set default values for descriptions
	if ( empty( $thinkup_homepage_section4_desc ) )
		$thinkup_homepage_section4_desc = 'The child theme "CroixRougeFrancaise" of Minamaze (Lite) theme it the perfect choice for any local website !';

	// Get page names for links
	if ( !empty( $thinkup_homepage_section4_link ) ) $thinkup_homepage_section4_link = get_permalink( $thinkup_homepage_section4_link );

	if ( is_front_page() or thinkup_check_ishome() ) {
		$my_content = '';
		if ( empty( $thinkup_homepage_sectionswitch ) or $thinkup_homepage_sectionswitch == '1' ) {

			$my_content .= '<article class="section4 one_third last">'.
			'<div class="section">'.
			'<div class="entry-header">';
			if ( empty( $thinkup_homepage_section4_image ) ) {
				$my_content .= '<img src="' . get_stylesheet_directory_uri() . '/images/slideshow/featured4.png' . '" alt="" />';
			} else {
				$my_content .= '<img src="' . $thinkup_homepage_section4_image[0] . '"  alt="" />';
			}
			$my_content .=	'</div>'.
			'<div class="entry-content">'.
			'<h3>' . esc_html( $thinkup_homepage_section4_title ) . '</h3>' . wpautop( do_shortcode ( esc_html( $thinkup_homepage_section4_desc ) ) ).
			'<p><a href="' . esc_url( $thinkup_homepage_section4_link ) . '" class="more-link themebutton">' . __( 'Read More', 'lan-thinkupthemes' ) . '</a></p>'.
			'</div>'.
			'</div>'.
			'</article>';

			//$my_content = '<div class="clearboth"></div></div></div>';
		}
		$content = preg_replace('`(<div[^>]+class="clearboth"[^>]*>)`', $my_content.'$1', $parent_content);
		$content = str_replace('class="section3 one_third last"', 'class="section3 one_third"', $content);
		$content = str_replace(' one_third', ' one_fourth', $content);
		echo $content;
	}
	else {
		echo $parent_content;
	}
}

