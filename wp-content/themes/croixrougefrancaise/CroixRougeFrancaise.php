<?php

/**
 * Thème Croix-Rouge
 * 
 * @author fraoult
 *
 */
class CroixRougeFrancaise
{
	
	/**
	 * Ajout des JS
	 */
	static function js()
	{
		wp_enqueue_script('crf_js', get_stylesheet_directory_uri().'/js/croixrouge.js');
	}

	/**
	 * Login
	 */
	static function login_head()
	{
		wp_enqueue_style( 'crf_admin_css', get_stylesheet_directory_uri().'/css/admin.css');
	}

	
} // END CLASS

add_action('wp_footer', 'CroixRougeFrancaise::js');
add_action('login_head', 'CroixRougeFrancaise::login_head');
