/**
 * ---------------------------------------
 * ---------------------------------------
 * Plugins JQuery
 * ---------------------------------------
 * ---------------------------------------
 */
jQuery.fn.afterResize = function(callback)
{
	var waitForFinalEvent = (function ()
	{
		var timers = {};
		return function (callback, ms, uniqueId)
		{
			if (!uniqueId)
			{
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId])
			{
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();
	
	jQuery(window).resize(function(){
		waitForFinalEvent(callback, 500, '___CRF_wait_resizing___');
	});
};


/**
 * ---------------------------------------
 * ---------------------------------------
 * Outils du theme
 * ---------------------------------------
 * ---------------------------------------
 */

CRF = {};

/**
 * uniqid
 * @param prefix
 */
CRF.uniqid = function(prefix)
{
	var salt = 0;
	var t = new Date().getTime() + Math.floor((Math.random()*100000)+1);
	var ts = ''; // t sous forme de chaine de caractère
	var id = ''; // Retour de la fonction

    // On se sert du prefix comme salt
	
    if (prefix)
    {
    	for(var i = 0; i<prefix.length; i++)
    	{
    		salt += prefix.charCodeAt(i);
    	}
        t += salt;
    }
    
    // pour eviter que id ne commence de la même façon à chaque fois
    // (à cause de Date::getTime() dont le résultat donne un poid fort identique pendant... longtemps)
    // on ajoute à chaque chiffre de t un autre chiffre aléatoire (0..9)
    
    ts = String(t);
    for (var i = 0; i < ts.length; i++)
    {
    	id += String( (Number(ts.charAt(i)) + (Math.floor(Math.random()*10)))%10  );
    }
    
    // id est considéré comme un nombre, puis convertis en hexa
    
    id = Number(id).toString(16).substring(0, 11);
    
    // Retour avec le prefix
    
    return prefix ? prefix + id : id;
};

/**
 * ---------------------------------------
 * ---------------------------------------
 * Gestion de la barre de menu
 * ---------------------------------------
 * ---------------------------------------
 */


var CRFMenu = function(selector, useadminbar)
{
	this.menu = jQuery(selector);
	this.posY = undefined;
	this.id = CRF.uniqid('menu_crf_');
	this.init(useadminbar);
};

/**
 * (ré)initialisation : place le CSS
 */
CRFMenu.prototype.init = function(useadminbar)
{

	var $ = jQuery;
	var hasClass   = this.menu.hasClass('fixed_crf_menu');
	if (hasClass)
	{
		this.menu.removeClass('fixed_crf_menu');
	}
	var wpadminbar = $('#wpadminbar');
	var offset     = useadminbar && (wpadminbar.length > 0) ? wpadminbar.height() : 0;
	var posX       = this.menu.offset().left;

	var css = $(
		'<style type="text/css" id="'+this.id+'">\n'+
		' '+this.menu.selector+'.fixed_crf_menu\n'+
		' {\n'+
		'  z-index: 9999;\n'+
		'  position: fixed;\n'+
		'  top: '+offset+'px;\n'+
		'  left: '+posX+'px;\n'+
		'  width: '+this.menu.css('width')+';\n'+
		'  height: '+this.menu.css('height')+';\n'+
		'  background-color: #FFF;\n'+
		'  box-shadow: 0 0 12px rgba(0,0,0,0.15);\n'+
		' }\n'+
		'</style>'
		);
	
	
	if ($('#'+this.id).length > 0)
	{
		$('#'+this.id).replaceWith(css);
	}
	else
	{
		$('body').prepend(css);
	}
	
	this.posY = this.menu.offset().top - (useadminbar && (wpadminbar.length > 0) ? wpadminbar.height() : 0);

	if (hasClass)
	{
		this.menu.addClass('fixed_crf_menu');
	}
	
};

/**
 * Action sur le scroll
 */
CRFMenu.prototype.action = function()
{
	var $ = jQuery;
	
	if ($(window).scrollTop() > this.posY)
	{
		this.menu.addClass('fixed_crf_menu');
	}
	else
	{
		this.menu.removeClass('fixed_crf_menu');
	}
};



jQuery(document).ready(function()
{
	var $               = jQuery;
	var menu_large      = new CRFMenu('#header-links', true);
	var menu_responsive = new CRFMenu('#header-responsive', false);
	
	$(window).scroll(function(){
		menu_large.action();
		menu_responsive.action();
	});
	
	$(window).resize(function(){
		menu_large.init(true);
		menu_responsive.init(false);
		menu_large.action(true);
		menu_responsive.action(false);
	});
	
	$('body').addClass('js_active');
	
	$('#header div.header-row div.row').click(function(){
		if ($(this).hasClass('row-top'))
		{
			$(this).removeClass('row-top').addClass('row-bottom');
			$('#logo h1').hide(250, function(){$('h1').css('display', '').addClass('responsive-hidden');});
			$('#pre-header').hide(250, function(){$('#pre-header').css('display', '').addClass('responsive-hidden');});
		}
		else
		{
			$(this).removeClass('row-bottom').addClass('row-top');
			$('#logo h1').css('display', 'none').removeClass('responsive-hidden').show(250);
			$('#pre-header').css('display', 'none').removeClass('responsive-hidden').show(250);
		}
	});
	
	$('#header div.header-row').appendTo('#logo');

	// Menu social network - responsive
	
	if ($('.lr-share-vertical-fix').length > 0)
	{
		var wait = setTimeout(function(){
			
			if ($('.lr-share-vertical-fix').length > 0)
			{
				clearTimeout(wait);
				$('.lr-share-vertical-fix').prependTo('footer');
			}
			
		}, 50);
	}

});





