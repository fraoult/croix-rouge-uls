<?php
/**
 * Menu gérant l'affichage des descriptions
 * @author fraoult
 * 
 */
class CRF_Nav_Menu extends Walker_Nav_Menu
{

	/**
	 * Afficher les descriptions ?
	 * @var boolean
	 */
	protected $_show_description = TRUE;
	
	/**
	 * Constructeur
	 * @param boolean $show_description
	 */
	function __construct($show_description=TRUE)
	{
		$this->_show_description = $show_description;
	}
	
	/**
	 * Ajouter les descriptions
	 * @see Walker_Nav_Menu::start_el()
	 */
	function start_el(&$output, $item, $depth, $args)
	{
		parent::start_el($output, $item, $depth, $args);

		if ($this->_show_description)
		{
			$output .= '<p class="description">'.$item->description.'</p>';
		}
	}
	
} // END CLASS
