<?php

// Couleurs de la charte graphique

$GLOBALS['crf_colors'] = array(
	'red'          => '#FF0000',
	'purple_light' => '#D10049',
	'purple_dark'  => '#69063A',
	'orange'       => '#FF5912',
	'green_dark'   => '#222E07',
	'green_light'  => '#586616',
	'beige_light'  => '#D9C883',
	'beige_dark'   => '#B28D47',
	'ocean_dark'   => '#295669',
	'ocean_light'  => '#6A95A3',
	'black'        => '#000000',
	'gray_dark'    => '#404040',
	'gray_light'   => '#8C8C8C',
	'white'        => '#FFFFFF'
);

// Inclusion des classes

include_once 'CroixRougeFrancaise.php';
include_once 'CRF_Menu.php';

include_once 'admin/functions.php';


load_theme_textdomain('redux-framework', get_template_directory() . '/languages' );

register_nav_menu('sections', __('Rubriques du site'));

